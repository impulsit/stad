#include-once
; ------------------------------------------------------------------------------
;
; AutoIt Version: 3.0
; Language:       English
; Description:    Functions that assist with Image Search
;                 Require that the ImageSearchDLL.dll be loadable
;
; ------------------------------------------------------------------------------

;===============================================================================
;
; Description:      Find the position of an image on the desktop
; Syntax:           _ImageSearchArea, _ImageSearch
; Parameter(s):
;                   $findImage - the image to locate on the desktop
;                   $tolerance - 0 for no tolerance (0-255). Needed when colors of
;                                image differ from desktop. e.g GIF
;                   $resultPosition - Set where the returned x,y location of the image is.
;                                     1 for centre of image, 0 for top left of image
;                   $x $y - Return the x and y location of the image
;
; Return Value(s):  On Success - Returns 1
;                   On Failure - Returns 0
;
; Note: Use _ImageSearch to search the entire desktop, _ImageSearchArea to specify
;       a desktop region to search
;
;===============================================================================

Global $_OffsetTX = 0
Global $_OffsetTY = 0
Global $_OffsetBX = 0
Global $_OffsetBY = 0


Func set_offset( $type = 0 )
   $_OffsetTX = Round( (@DesktopWidth - 1920) / 2, 0 )
   $_OffsetTY = Round( (@DesktopHeight - 1080) / 2, 0 )
   $_OffsetBX = Round( (@DesktopWidth - 1920) / 2, 0 ) + 1920
   $_OffsetBY = Round( (@DesktopHeight - 1080) / 2, 0 ) + 1080

   If( IsArray($type) ) Then
	  $_OffsetTX = $_OffsetTX + $type[0]
	  $_OffsetTY = $_OffsetTY + $type[1]
	  $_OffsetBX = $_OffsetBX - (1920 - $type[2])
	  $_OffsetBY = $_OffsetBY - (1080 - $type[3])
	  Return
   EndIf

   ; Box in Mitte
   If( $type = 1 ) Then
	  $_OffsetTX = $_OffsetTX + 730
	  $_OffsetTY = $_OffsetTY + 350
	  $_OffsetBX = $_OffsetBX - 730
	  $_OffsetBY = $_OffsetBY - 350
   EndIf

   ; Skill Leiste
   If( $type = 2 ) Then
	  $_OffsetTX = $_OffsetTX + 800
	  $_OffsetTY = $_OffsetTY + 800
	  $_OffsetBX = $_OffsetBX - 600
	  $_OffsetBY = $_OffsetBY -  50
   EndIf

   ; Verlassen nach Kampf
   If( $type = 3 ) Then
	  $_OffsetTX = $_OffsetTX + 800
	  $_OffsetTY = $_OffsetTY + 600
	  $_OffsetBX = $_OffsetBX - 630 ;800
	  $_OffsetBY = $_OffsetBY - 250
   EndIf

   ; Map Rechts oben
   If( $type = 4 ) Then
	  $_OffsetTX = $_OffsetTX + 1600
	  $_OffsetTY = $_OffsetTY +    0
	  $_OffsetBX = $_OffsetBX -    0
	  $_OffsetBY = $_OffsetBY -  600
   EndIf

   ; Chat Box
   If( $type = 5 ) Then
	  $_OffsetTX = $_OffsetTX +    0
	  $_OffsetTY = $_OffsetTY +  700
	  $_OffsetBX = $_OffsetBX - 1500
	  $_OffsetBY = $_OffsetBY -    0
   EndIf
EndFunc

Func _ImageSearch( $findImage, $resultPosition, ByRef $x, ByRef $y, $tolerance, $type = 0 )
   set_offset( $type )

   $iOffsetTopLeftX = $_OffsetTX
   $iOffsetTopLeftY = $_OffsetTY
   $iOffsetBottomRightX = $_OffsetBX
   $iOffsetBottomRightY =$_OffsetBY

   ;return _ImageSearchArea($findImage,$resultPosition,0,0,@DesktopWidth,@DesktopHeight,$x,$y,$tolerance)
   return _ImageSearchArea($findImage,$resultPosition,$iOffsetTopLeftX,$iOffsetTopLeftY,$iOffsetBottomRightX,$iOffsetBottomRightY,$x,$y,$tolerance)
EndFunc

Func _ImageSearchArea($findImage,$resultPosition,$x1,$y1,$right,$bottom, ByRef $x, ByRef $y, $tolerance)
    ;MsgBox(0,"asd","" & $x1 & " " & $y1 & " " & $right & " " & $bottom)
    if $tolerance>0 then $findImage = "*" & $tolerance & " " & $findImage
    $result = DllCall("ImageSearchDLL.dll","str","ImageSearch","int",$x1,"int",$y1,"int",$right,"int",$bottom,"str",$findImage)

    ; If error exit
    if $result[0]="0" then return 0

    ; Otherwise get the x,y location of the match and the size of the image to
    ; compute the centre of search
    $array = StringSplit($result[0],"|")

   $x=Int(Number($array[2]))
   $y=Int(Number($array[3]))
   if $resultPosition=1 then
      $x=$x + Int(Number($array[4])/2)
      $y=$y + Int(Number($array[5])/2)
   endif
   return 1
EndFunc

;===============================================================================
;
; Description:      Wait for a specified number of seconds for an image to appear
;
; Syntax:           _WaitForImageSearch, _WaitForImagesSearch
; Parameter(s):
;                   $waitSecs  - seconds to try and find the image
;                   $findImage - the image to locate on the desktop
;                   $tolerance - 0 for no tolerance (0-255). Needed when colors of
;                                image differ from desktop. e.g GIF
;                   $resultPosition - Set where the returned x,y location of the image is.
;                                     1 for centre of image, 0 for top left of image
;                   $x $y - Return the x and y location of the image
;
; Return Value(s):  On Success - Returns 1
;                   On Failure - Returns 0
;
;
;===============================================================================
Func _WaitForImageSearch($findImage,$waitSecs,$resultPosition, ByRef $x, ByRef $y,$tolerance)
    $waitSecs = $waitSecs * 1000
    $startTime=TimerInit()
    While TimerDiff($startTime) < $waitSecs
        sleep(100)
        $result=_ImageSearch($findImage,$resultPosition,$x, $y,$tolerance)
        if $result > 0 Then
            return 1
        EndIf
    WEnd
    return 0
EndFunc

;===============================================================================
;
; Description:      Wait for a specified number of seconds for any of a set of
;                   images to appear
;
; Syntax:           _WaitForImagesSearch
; Parameter(s):
;                   $waitSecs  - seconds to try and find the image
;                   $findImage - the ARRAY of images to locate on the desktop
;                              - ARRAY[0] is set to the number of images to loop through
;                                ARRAY[1] is the first image
;                   $tolerance - 0 for no tolerance (0-255). Needed when colors of
;                                image differ from desktop. e.g GIF
;                   $resultPosition - Set where the returned x,y location of the image is.
;                                     1 for centre of image, 0 for top left of image
;                   $x $y - Return the x and y location of the image
;
; Return Value(s):  On Success - Returns the index of the successful find
;                   On Failure - Returns 0
;
;
;===============================================================================
Func _WaitForImagesSearch($findImage,$waitSecs,$resultPosition, ByRef $x, ByRef $y,$tolerance)
    $waitSecs = $waitSecs * 1000
    $startTime=TimerInit()
    While TimerDiff($startTime) < $waitSecs
        for $i = 1 to $findImage[0]
            sleep(100)
            $result=_ImageSearch($findImage[$i],$resultPosition,$x, $y,$tolerance)
            if $result > 0 Then
                return $i
            EndIf
        Next
    WEnd
    return 0
EndFunc