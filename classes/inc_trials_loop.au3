#include "base.au3"

Debug( "--------------" )
Debug( "--- TRIALS ---" )
Debug( "--------------" )

load_config_file()

$bStopTrials = False
$fight_type = 3 ; Farmen
$bInSektor = True

reset_mouse()
If( $bInSektor ) Then
	switch_to_weapon_offi( $_fragezeichen_weapon1, $_fragezeichen_weapon2, $_fragezeichen_offi, $_fragezeichen_repair )
	If( $_fragezeichen_shuttles ) Then
		load_shuttles( $_fragezeichen_heal_shuttles )
	Else
		unload_shuttles()
	EndIf
	change_weapon_set( $_fragezeichen_weapon_set )
EndIf

If( wait_for_window( "trials/icon_trials", "trials/angriff" ) ) Then
	While (Not $bStopTrials) And (Not event_starting_soon())
		If( check_dc() ) Then
			$bStopTrials = True
			$iLastTrials = 0
		EndIf

		If( search_pattern( "kampf/att_start" ) ) Then do_fight( $fight_type )

		If( search_pattern( "trials/finish" ) ) Then
			Debug( "[TRIALS] Ende" )
			$bStopTrials = True
		EndIf

		If( Not $bStopTrials ) Then
			If( search_pattern( "trials/angriff" ) ) Then
				Debug( "[TRIALS] Angriff" )
				mouse_move_relative( 0, 0, True )
			EndIf

			wait_sec( 2 )
		EndIf
	WEnd
EndIf

If( search_pattern( "trials/abholen" ) ) Then mouse_move_relative( 0, 0, True )

close_all_windows()

Debug( "-------------------" )
Debug( "--- TRIALS ENDE ---" )
Debug( "-------------------" )
