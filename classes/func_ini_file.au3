#include-once

Global $_show_debug_window, $_skill[10][10], $_max_skills, $_disable_reload_window, $_reload_wait_timer
Global $_send_shuttle_next_side, $_send_shuttle_postition
Global $_stop_boss_06_1, $_stop_boss_06_2, $_stop_boss_12_1, $_stop_boss_12_2, $_stop_boss_19_1, $_stop_boss_19_2, $_wait_sec_06, $_wait_sec_12, $_wait_sec_19
Global $_fragezeichen_use_deut
Global $_fragezeichen_offi,			$_fragezeichen_weapon1,			$_fragezeichen_weapon2,			$_fragezeichen_shuttles,		$_fragezeichen_heal_shuttles,			$_fragezeichen_repair,		$_fragezeichen_weapon_set			, $_fragezeichen_collect_res
Global $_sys_offi,							$_sys_weapon1,							$_sys_weapon2,							$_sys_shuttles,							$_sys_heal_shuttles,							$_sys_repair,							$_sys_weapon_set							, $_sys_repair_time
Global $_3v3_offi,							$_3v3_weapon1,							$_3v3_weapon2,							$_3v3_shuttles,							$_3v3_heal_shuttles,							$_3v3_repair,							$_3v3_weapon_set
Global $_herausforderung_offi, 	$_herausforderung_weapon1, 	$_herausforderung_weapon2, 	$_herausforderung_shuttles, $_herausforderung_heal_shuttles, 	$_herausforderung_repair, $_herausforderung_weapon_set
Global $_shuttle_offi,					$_shuttle_weapon1,					$_shuttle_weapon2,					$_shuttle_shuttles,					$_shuttle_heal_shuttles,					$_shuttle_repair,					$_shuttle_weapon_set
Global $_interstellar_offi,			$_interstellar_weapon1,			$_interstellar_weapon2,			$_interstellar_shuttles,		$_interstellar_heal_shuttles,			$_interstellar_repair,		$_interstellar_weapon_set
Global $_boss_offi,							$_boss_weapon1,							$_boss_weapon2,							$_boss_shuttles,						$_boss_heal_shuttles,							$_boss_repair,						$_boss_weapon_set
Global $_beute_offi,						$_beute_weapon1,						$_beute_weapon2,						$_beute_shuttles,						$_beute_heal_shuttles,						$_beute_repair,						$_beute_weapon_set
Global $_staffel_buy_vip,				$_staffel_buy_kit5,					$_staffel_spend_syn

Func load_config_file()
	$strIniFile = "classes/config.ini"

	; Skills
	$_max_skills = IniRead( $strIniFile, "Skills", "max_skills", 0 )
	For $i = 1 To $_max_skills
		$_skill[$i-1][0] = IniRead( $strIniFile, "Skills", "skill_" & $i, "" )
		$_skill[$i-1][1] = IniRead( $strIniFile, "Skills", "key_" & $i, "" )
	Next

	; General
	$_show_debug_window = boolIniRead( $strIniFile, "General", "show_debug_window", "True" )
	$_disable_reload_window = boolIniRead( $strIniFile, "General", "disable_reload_window", "True" )
	$_reload_wait_timer = IniRead( $strIniFile, "General", "reload_wait_timer", 2 )


	; Send_Shuttle
	$_send_shuttle_next_side = boolIniRead( $strIniFile, "Send_Shuttle", "next_side", "True" )
	$_send_shuttle_postition = IniRead( $strIniFile, "Send_Shuttle", "position", 2 )

	; Boss
	$sektion = "Boss"
	$_stop_boss_06_1 = IniRead( $strIniFile, $sektion, "stop_boss_06_1", 12 )
	$_stop_boss_06_2 = IniRead( $strIniFile, $sektion, "stop_boss_06_2", 9 )
	$_stop_boss_12_1 = IniRead( $strIniFile, $sektion, "stop_boss_12_1", 7 )
	$_stop_boss_12_2 = IniRead( $strIniFile, $sektion, "stop_boss_12_2", 7 )
	$_stop_boss_19_1 = IniRead( $strIniFile, $sektion, "stop_boss_19_1", 0 )
	$_stop_boss_19_2 = IniRead( $strIniFile, $sektion, "stop_boss_19_2", 0 )
	$_wait_sec_06 = IniRead( $strIniFile, $sektion, "wait_sec_06", 1200 )
	$_wait_sec_12 = IniRead( $strIniFile, $sektion, "wait_sec_12", 20 )
	$_wait_sec_19 = IniRead( $strIniFile, $sektion, "wait_sec_19", 0 )
	$_boss_offi = IniRead( $strIniFile, $sektion, "offi", "clay_lucas" )
	$_boss_weapon1 = IniRead( $strIniFile, $sektion, "weapon1", "emp_70" )
	$_boss_weapon2 = IniRead( $strIniFile, $sektion, "weapon2", "turm_80" )
	$_boss_shuttles = boolIniRead( $strIniFile, $sektion, "shuttles", True )
	$_boss_heal_shuttles = boolIniRead( $strIniFile, $sektion, "heal_shuttles", False )
	$_boss_repair = boolIniRead( $strIniFile, $sektion, "repair", False )
	$_boss_weapon_set = IniRead( $strIniFile, $sektion, "weapon_set", 1 )

	; Fragezeichen
	$sektion = "Fragezeichen"
	$_fragezeichen_offi = IniRead( $strIniFile, $sektion, "offi", "jakob_karel" )
	$_fragezeichen_weapon1 = IniRead( $strIniFile, $sektion, "weapon1", "emp_70" )
	$_fragezeichen_weapon2 = IniRead( $strIniFile, $sektion, "weapon2", "disruptorkanone_78" )
	$_fragezeichen_shuttles = boolIniRead( $strIniFile, $sektion, "shuttles", False )
	$_fragezeichen_heal_shuttles = boolIniRead( $strIniFile, $sektion, "heal_shuttles", False )
	$_fragezeichen_repair = boolIniRead( $strIniFile, $sektion, "repair", False )
	$_fragezeichen_collect_res = boolIniRead( $strIniFile, $sektion, "collect_res", False )
	$_fragezeichen_use_deut = boolIniRead( $strIniFile, $sektion, "use_deut", False )
	$_fragezeichen_weapon_set = IniRead( $strIniFile, $sektion, "weapon_set", 2 )

	; Sys
	$sektion = "Sys"
	$_sys_offi = IniRead( $strIniFile, $sektion, "offi", "penelope_patton" )
	$_sys_weapon1 = IniRead( $strIniFile, $sektion, "weapon1", "emp_70" )
	$_sys_weapon2 = IniRead( $strIniFile, $sektion, "weapon2", "disruptorkanone_78" )
	$_sys_shuttles = boolIniRead( $strIniFile, $sektion, "shuttles", True )
	$_sys_heal_shuttles = boolIniRead( $strIniFile, $sektion, "heal_shuttles", True )
	$_sys_repair = boolIniRead( $strIniFile, $sektion, "repair", False )
	$_sys_weapon_set = IniRead( $strIniFile, $sektion, "weapon_set", 2 )
	$_sys_repair_time = IniRead( $strIniFile, $sektion, "repair_time", 2 )

	; 3v3
	$sektion = "3v3"
	$_3v3_offi = IniRead( $strIniFile, $sektion, "offi", "penelope_patton" )
	$_3v3_weapon1 = IniRead( $strIniFile, $sektion, "weapon1", "emp_70" )
	$_3v3_weapon2 = IniRead( $strIniFile, $sektion, "weapon2", "disruptorkanone_78" )
	$_3v3_shuttles = boolIniRead( $strIniFile, $sektion, "shuttles", True )
	$_3v3_heal_shuttles = boolIniRead( $strIniFile, $sektion, "heal_shuttles", True )
	$_3v3_repair = boolIniRead( $strIniFile, $sektion, "repair", True )
	$_3v3_weapon_set = IniRead( $strIniFile, $sektion, "weapon_set", 2 )

	; Herausforderung
	$sektion = "Herausforderung"
	$_herausforderung_offi = IniRead( $strIniFile, $sektion, "offi", "penelope_patton" )
	$_herausforderung_weapon1 = IniRead( $strIniFile, $sektion, "weapon1", "emp_70" )
	$_herausforderung_weapon2 = IniRead( $strIniFile, $sektion, "weapon2", "disruptorkanone_78" )
	$_herausforderung_shuttles = boolIniRead( $strIniFile, $sektion, "shuttles", False )
	$_herausforderung_heal_shuttles = boolIniRead( $strIniFile, $sektion, "heal_shuttles", False )
	$_herausforderung_repair = boolIniRead( $strIniFile, $sektion, "repair", False )
	$_herausforderung_weapon_set = IniRead( $strIniFile, $sektion, "weapon_set", 2 )

	; Shuttle
	$sektion = "Shuttle"
	$_shuttle_offi = IniRead( $strIniFile, $sektion, "offi", "penelope_patton" )
	$_shuttle_weapon1 = IniRead( $strIniFile, $sektion, "weapon1", "emp_70" )
	$_shuttle_weapon2 = IniRead( $strIniFile, $sektion, "weapon2", "disruptorkanone_78" )
	$_shuttle_shuttles = boolIniRead( $strIniFile, $sektion, "shuttles", False )
	$_shuttle_heal_shuttles = boolIniRead( $strIniFile, $sektion, "heal_shuttles", False )
	$_shuttle_repair = boolIniRead( $strIniFile, $sektion, "repair", False )
	$_shuttle_weapon_set = IniRead( $strIniFile, $sektion, "weapon_set", 2 )

	; Interstellar
	$sektion = "Interstellar"
	$_interstellar_offi = IniRead( $strIniFile, $sektion, "offi", "penelope_patton" )
	$_interstellar_weapon1 = IniRead( $strIniFile, $sektion, "weapon1", "emp_70" )
	$_interstellar_weapon2 = IniRead( $strIniFile, $sektion, "weapon2", "disruptorkanone_78" )
	$_interstellar_shuttles = boolIniRead( $strIniFile, $sektion, "shuttles", False )
	$_interstellar_heal_shuttles = boolIniRead( $strIniFile, $sektion, "heal_shuttles", False )
	$_interstellar_repair = boolIniRead( $strIniFile, $sektion, "repair", False )
	$_interstellar_weapon_set = IniRead( $strIniFile, $sektion, "weapon_set", 2 )

	; Beute
	$sektion = "Beute"
	$_beute_offi = IniRead( $strIniFile, $sektion, "offi", "jakob_karel" )
	$_beute_weapon1 = IniRead( $strIniFile, $sektion, "weapon1", "emp_70" )
	$_beute_weapon2 = IniRead( $strIniFile, $sektion, "weapon2", "disruptorkanone_78" )
	$_beute_shuttles = boolIniRead( $strIniFile, $sektion, "shuttles", False )
	$_beute_heal_shuttles = boolIniRead( $strIniFile, $sektion, "heal_shuttles", False )
	$_beute_repair = boolIniRead( $strIniFile, $sektion, "repair", False )
	$_beute_weapon_set = IniRead( $strIniFile, $sektion, "weapon_set", 2 )
	
	; Staffel
	$sektion = "Staffel"
	$_staffel_buy_vip = boolIniRead( $strIniFile, $sektion, "buy_vip", False )
	$_staffel_buy_kit5 = boolIniRead( $strIniFile, $sektion, "buy_kit5", False )
	$_staffel_spend_syn = boolIniRead( $strIniFile, $sektion, "spend_syn", False )
EndFunc

Func boolIniRead( $strIniFile, $sektion, $tag, $default )
	$b = False
	$str = IniRead( $strIniFile, $sektion, $tag, $default )
	If( $str = "True" ) Then $b = True

	Return( $b )
EndFunc
