#include "base.au3"

Debug( "--------------------" )
Debug( "--- INTERSTELLAR ---" )
Debug( "--------------------" )

load_config_file()

$bStopInterstellar = False
$fight_type = 6
$bInSektor = True

reset_mouse()
If( $bInSektor ) Then
	switch_to_weapon_offi( $_interstellar_weapon1, $_interstellar_weapon2, $_interstellar_offi, $_interstellar_repair )
	If( $_interstellar_shuttles ) Then
		load_shuttles( $_interstellar_heal_shuttles )
	Else
		unload_shuttles()
	EndIf
	change_weapon_set( $_interstellar_weapon_set )
EndIf

; Move to Sektor Angriff
$strDo = "19"
;If( Not wait_for_window( "inter/back", "inter/stufe_" & $strDo ) ) Then $bStopInterstellar = True

If( wait_for_window( "inter/icon_interstellar", "inter/title_interstellar" ) ) Then
	While( (Not $bStopInterstellar) And (Not event_starting_soon()) )
		If( check_dc() ) Then
			$bStopInterstellar = True
			$iLastInterstellar = 0
		EndIf

		If( search_pattern( "kampf/att_start" ) ) Then do_fight( $fight_type )

		If( search_pattern( "inter/finish" ) ) Then
			Debug( "[INTER] Ende" )
			$bStopInterstellar = True
		EndIf

		If( Not $bStopInterstellar) Then
			If( search_pattern( "inter/angriff" ) ) Then
			Debug( "[INTER] Angriff" )
			mouse_move_relative( 0, 0, True )
			EndIf
			
			wait_sec( 2 )
		EndIf
	WEnd
EndIf

close_all_windows()

Debug( "-------------------------" )
Debug( "--- INTERSTELLAR ENDE ---" )
Debug( "-------------------------" )
