#include "base.au3"

Debug( "---------------" )
Debug( "--- WINGMAN ---" )
Debug( "---------------" )

load_config_file()

$fight_type = 5 ; Shuttle

$bInSektor = True
$bInShuttle = False
$bStopShuttle = False
$iFailed = 0
$iStage = 0
$bNextStage = True
$bShuttleFinished = False
$iResetCounter = 0
$iCountAtt = 0
$iStarttime = get_timestamp()

reset_mouse()
If( $bInSektor ) Then
	switch_to_weapon_offi( $_shuttle_weapon1, $_shuttle_weapon2, $_shuttle_offi, $_shuttle_repair )
	If( $_shuttle_shuttles ) Then
		load_shuttles( $_shuttle_heal_shuttles )
	Else
		unload_shuttles()
	EndIf
	change_weapon_set( $_shuttle_weapon_set )
EndIf

While( (Not $bShuttleFinished) And (Not event_starting_soon()) )
	If( check_dc() ) Then
		$bShuttleFinished = True
		$bStopShuttle = True
		$iLastShuttle = 0
	EndIf

	If( ($iCountAtt > 75) Or ((get_timestamp() - $iStarttime) > 120) ) Then
		$iCountAtt = 0
		$iStarttime = get_timestamp()
		
		reload_window()
	EndIf
	
	If( search_pattern( "kampf/verlassen" ) ) Then
		Debug( "[ATT] Kampf verlassen Failsafe" )
		mouse_move_relative( 0, 0, true )

		close_all_windows()
	EndIf

	If( search_pattern( "shuttle/icon_shuttle" ) ) Then
		mouse_move_relative( 0, 0, True )
		$bInSektor = False
		wait_sec( 1 )
	EndIf

	If( ($iStage = 0 ) And $bNextStage And search_pattern( "shuttle/shuttle_alpha" ) ) Then
		Debug( "[SHU] Start Stage Alpha" )
		mouse_move_relative( -70, 260, True )
		$iFailed = 0
		$bInShuttle = True
		$bStopShuttle = False
		$iStage = 1
		$bNextStage = False
	EndIf

	If( ($iStage = 1 ) And $bNextStage And search_pattern( "shuttle/shuttle_beta" ) ) Then
		Debug( "[SHU] Start Stage Beta" )
		mouse_move_relative( -70, 260, True )
		$iFailed = 0
		$bInShuttle = True
		$bStopShuttle = False
		$iStage = 2
		$bNextStage = False
	EndIf

	If( ($iStage = 2 ) And $bNextStage And search_pattern( "shuttle/shuttle_gamma" ) ) Then
		Debug( "[SHU] Start Stage Gamma" )
		mouse_move_relative( -70, 260, True )
		$iFailed = 0
		$bInShuttle = True
		$bStopShuttle = False
		$iStage = 3
		$bNextStage = False
	EndIf

	If( ($iStage = 3 ) And $bNextStage ) Then
		If( search_pattern( "shuttle/next" ) ) Then mouse_move_relative( 0, 0, True )
		If( search_pattern( "shuttle/shuttle_nu" ) ) Then
			Debug( "[SHU] Start Stage Nu" )
			mouse_move_relative( -70, 260, True )
			$iFailed = 0
			$bInShuttle = True
			$bStopShuttle = False
			$iStage = 4
			$bNextStage = False
		EndIf
	EndIf

	If( ($iStage = 4 ) And $bNextStage ) Then
		If( search_pattern( "shuttle/next" ) ) Then mouse_move_relative( 0, 0, True )
		If( search_pattern( "shuttle/next" ) ) Then mouse_move_relative( 0, 0, True )
		If( search_pattern( "shuttle/shuttle_zeta" ) ) Then
			Debug( "[SHU] Start Stage Zeta" )
			mouse_move_relative( -70, 260, True )
			$iFailed = 0
			$bInShuttle = True
			$bStopShuttle = False
			$iStage = 5
			$bNextStage = False
		EndIf
	EndIf

	If( ($iStage >= 5 ) And $bNextStage ) Then
		Debug( "[SHU] END" )
		$bShuttleFinished = True
	EndIf

	If( $bInShuttle ) Then
		Debug( "[SHU] Start Stage: " & $iStage )
		$iAttCounter = 0
		While( (Not $bStopShuttle) And (Not event_starting_soon()) )
			$iStarttime = get_timestamp()
			
			If( check_dc() ) Then
				$bShuttleFinished = True
				$bStopShuttle = True
				$iLastShuttle = 0
			EndIf
			
			If( search_pattern( "kampf/verlassen" ) ) Then
				Debug( "[ATT] Kampf verlassen Failsafe" )
				mouse_move_relative( 0, 0, true )
			EndIf

			If( search_pattern( "kampf/att_start" ) ) Then
				$iAttCounter = 0
				$iCountAtt = $iCountAtt + 1
				$bFailed = do_fight( $fight_type, $iStage )
				If( $bFailed ) Then
					$iFailed = $iFailed + 1
					Debug( "[ATT] Failed: " & $iFailed )
				Else
					$iFailed = 0
				EndIf
			EndIf

			If( $iFailed >= 3 ) Then
				Debug( "[SHU] Failed max" )
				$bNextStage = True

				close_all_windows()

				$bInShuttle = False
				$bStopShuttle = True
				$bInSektor = True
			EndIf

			If( search_pattern( "shuttle/reset_free" ) ) Then ; ($iResetCounter < 5) And
				Debug( "[SHU] Reset" )
				$iResetCounter = $iResetCounter + 1
				mouse_move_relative( 0, 0, True )
				wait_sec( 1 )
			EndIf

			If( event_starting_soon() ) Then
				$bStopShuttle = True
				$bShuttleFinished = True
			EndIf

			If( (Not $bStopShuttle) And search_pattern( "shuttle/shuttle_angriff" ) ) Then
				Debug( "[SHU] Angriff" )
				mouse_move_relative( 0, 0, True, True )
				$iAttCounter = $iAttCounter + 1
				If( $iAttCounter >= 10 ) Then $iFailed = 99
			EndIf

			If( Not $bStopShuttle ) Then wait_sec( 0.5 )
		WEnd
	EndIf

	If( Not $bShuttleFinished ) Then wait_sec( 2 )
WEnd

close_all_windows()
reload_window()

Debug( "--------------------" )
Debug( "--- WINGMAN ENDE ---" )
Debug( "--------------------" )
