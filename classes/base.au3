#include-once
#include <Date.au3>
#include <Array.au3>
#include <Constants.au3>
#include <GUIConstants.au3>
#include <ScreenCapture.au3>
#include "ImageSearch.au3"
#include "functions.au3"

HotKeySet("^q", "end_script") ; CTRL + q
HotKeySet("^{F12}", "end_script") ; CTRL + F12
HotKeySet("^r", "collect_res") ; CTRL + r
HotKeySet("^l", "load_shuttles") ; CTRL + l
HotKeySet("^u", "unload_shuttles") ; CTRL + u
HotKeySet("^o", "open_signs") ; CTRL + o
HotKeySet("^p", "pause") ; CTRL + p
HotKeySet("^s", "hotkey_do_screenshot") ; CTRL + s
HotKeySet("^b", "repair_fleet") ; CTRL + b

Global $_x = 0
Global $_y = 0
Global $_InFight = False
Global $_standalone = False
Global $_bPause = False

load_config_file()
start_browser()

Func end_script()
   Exit
EndFunc
