#include "base.au3"

Debug( "-----------------------" )
Debug( "--- HERAUSFORDERUNG ---" )
Debug( "-----------------------" )

load_config_file()

$fight_type = 2 ; Herausforderungen

$bInSektor = True
$bInHeraus = False
$bStopHeraus = False
$iFailed = 0
$iFailCounter = 5
$iStartTime = get_timestamp()
$iAttCounter = 0

reset_mouse()
If( $bInSektor ) Then
	switch_to_weapon_offi( $_herausforderung_weapon1, $_herausforderung_weapon2, $_herausforderung_offi, $_herausforderung_repair )
	If( $_herausforderung_shuttles ) Then
		load_shuttles( $_herausforderung_heal_shuttles )
	Else
		unload_shuttles()
	EndIf
	change_weapon_set( $_herausforderung_weapon_set )
EndIf

While( $bInSektor	And (Not event_starting_soon()) )
	Debug( "[HER] Suche Icon Herausforderung" )
	If( search_pattern( "heraus/icon_heraus" ) ) Then
		Debug( "[HER] Klick Herausforderung" )
		mouse_move_relative( 0, 0, True )
		wait_sec( 2 )

		Debug( "[HER] Klick Normal" )
		If( search_pattern( "heraus/her_normal" ) ) Then
			mouse_move_relative( 0, 0, True )

			$bInSektor = False
			$bInHeraus = True
		EndIf
	EndIf
	wait_sec( 2 )
WEnd

While( (Not $bStopHeraus) And (Not event_starting_soon()) )
	If( check_dc() ) Then
		$bStopHeraus = True
		$iLastHeraus = 0
	EndIf

	If( search_pattern( "kampf/att_ueberladung" ) ) Then
		$bFailed = do_fight( $fight_type )
		If( $bFailed ) Then
			$iFailed = $iFailed + 1
			Debug( "[ATT] Failed: " & $iFailed )
		Else
			$iFailed = 0
		EndIf
	EndIf

	If( search_pattern( "kampf/verlassen" ) ) Then mouse_move_relative( 0, 0, true )

	If( Not $bStopHeraus ) Then
		If( ($iFailed >= $iFailCounter) Or search_pattern( "heraus/her_finished" ) ) Then
			$bRestart = False

			If( (Not $bRestart) And search_pattern( "heraus/her_normal" ) ) Then
				Debug( "[HER]Normal Beendet" )
				mouse_move_relative( 760, 390, True )
				wait_sec( 2 )
				If( search_pattern( "heraus/her_schwer", 0, 90 ) ) Then
					Debug( "[HER]Start Schwer" );
					mouse_move_relative( 0, 0, True )
					$bRestart = True
				EndIf
			EndIf

			If( (Not $bRestart) And search_pattern( "heraus/her_schwer", 0, 90 ) ) Then
				Debug( "[HER]Schwer Beendet" )
				mouse_move_relative( 760, 390, True )
				wait_sec( 2 )
				If( search_pattern( "heraus/her_experte" ) ) Then
					Debug( "[HER]Start Experte" );
					mouse_move_relative( 0, 0, True )
					$bRestart = True
				EndIf
			EndIf

			If( (Not $bRestart) And (search_pattern( "heraus/her_experte" ) Or ($iFailed >= $iFailCounter)) ) Then
				Debug( "[HER]Experte Beendet / Failed: " & $iFailed )
				If( $iFailed >= $iFailCounter ) Then $bStopHeraus = True
				If( Not search_pattern( "heraus/angriff" ) ) Then $bStopHeraus = True
			EndIf
		EndIf
	EndIf

	$bAttack = True
	If( (Not $bStopHeraus) And search_pattern( "heraus/her_experte" ) And search_pattern( "heraus/her_finished" ) ) Then
		Debug( "[HER]Reset?" )
		If( (@WDAY <> 1) And search_pattern( "heraus/her_reset_0_3" ) ) Then $bStopHeraus = True
		If( (@WDAY =	1) And search_pattern( "heraus/her_reset_0_3" ) And ($iFailed >= $iFailCounter) ) Then $bStopHeraus = True

		If( (Not $bStopHeraus) And search_pattern( "heraus/her_reset" ) ) Then
			Debug( "[HER]Ruecksetzen" )
			mouse_move_relative( -240, 0, True )
			wait_sec( 0.5 )

			mouse_move_relative( 320, -100, True )
			wait_sec( 0.5 )
			$iFailed = 0
		EndIf
	EndIf

	If( $bAttack And (Not $bStopHeraus) And ($iFailed < $iFailCounter) And search_pattern( "heraus/angriff" ) ) Then
		Debug( "[HER]Angriff moeglich" )
		mouse_move_relative( 0, 17, True )
		
		$iStartTime = get_timestamp()
	EndIf
	
	If( (get_timestamp() - $iStarttime) > 300 ) Then
		Debug( "[HER] kein Angriff moeglich -> Abbruch" )
		
		$bStopHeraus = True
		$iLastHeraus = 0
	EndIf

	If( Not $bStopHeraus ) Then wait_sec( 2 )
WEnd

close_all_windows()
$bInSektor = True
$bInHeraus = False

reload_window()

Debug( "----------------------------" )
Debug( "--- HERAUSFORDERUNG ENDE ---" )
Debug( "----------------------------" )
