#include "base.au3"

Debug( "------------" )
Debug( "--- CSSS ---" )
Debug( "------------" )

load_config_file()

$bInSys = False
$bStopSys = False
$iSleep = 0.1
$iPos = 1
$iLastPos = 1
$iLastRepair = get_timestamp()
$iJoinTime = -1

If( search_pattern( "sys/sys_start_into" ) ) Then
	$bInSys = True
	$iSleep = 2
EndIf

If( Not $bInSys ) Then
	reset_mouse()
	switch_to_weapon_offi( $_sys_weapon1, $_sys_weapon2, $_sys_offi, $_sys_repair )
	If( $_sys_shuttles ) Then
		load_shuttles( $_sys_heal_shuttles )
	Else
		unload_shuttles()
	EndIf
	change_weapon_set( $_sys_weapon_set )
EndIf

While (Not $bStopSys)
	$hour = Int( @HOUR )
	$min = Int( @MIN )
	If( ($min >= 2) And ($min <= 40) ) Then 
		$iSleep = 2
		check_dc()
	EndIf
		
	If( search_pattern( "sys/sys_start_into" ) ) Then
		$bInSys = True
	Else
		$bInSys = False
	EndIf

	; Sys Anmeldung
	If( Not $bInSys ) Then
		Debug( "[SYS] Suche Sys Start" )
		If( search_pattern( "sys/sys_start" ) Or search_pattern( "sys/sys_start_text" ) ) Then
			Debug( "[SYS] Suche Sys Betreten" )
			if( $iJoinTime = -1 ) Then $iJoinTime = get_timestamp()

			If( search_pattern( "sys/center_ship" ) ) Then
				Debug( "[SYS] Sys beitreten" )
				$iJoinTime = get_timestamp()

				mouse_move_relative(	130, -35, True )
				mouse_move_relative( -180, 130, True )
				wait_sec( 1 )
			Else
				If( $iJoinTime > 10 ) Then ; schon beitreten geklickt?
					If( (get_timestamp() - $iJoinTime) >  20 ) Then center_fleet()
					If( (get_timestamp() - $iJoinTime) >  60 ) Then reload_window()
				EndIf
			EndIf
		EndIf
	EndIf

	; In Sys Screen?
	If( $bInSys ) Then
		close_all_windows()

		$bDo = True
		If( $iSleep > 1 ) Then
			If( search_pattern( "sys/sys_autokampf" ) ) Then
				Debug( "[SYS] Auto-Kampf aktivieren" )
				mouse_move_relative( 3, 3, True )
			EndIf
			If( search_pattern( "sys/sys_in_repair" )	) Then
				Debug( "[SYS] wird Repariert" )
				$bDo = False
				$iLastRepair = get_timestamp()
			EndIf
		EndIf
			
		If( search_pattern( "sys/sys_im_kampf" )	) Then
			Debug( "[SYS] im Kampf" )
			$bDo = False
			$iSleep = 2
		EndIf

		If( $bDo ) Then
			If( $iPos <> $iLastPos ) Then
				$iLastPos = $iPos
				If( search_pattern( "sys/sys_flotte" ) ) Then
					Debug( "[SYS] Center" )
					mouse_move_relative( 0, 0, True )
				EndIf
			EndIf

			$iPos = -1
			If( $iSleep <= 1 ) Then $iPos = 1 ; Basis
			If( $iPos = -1 ) Then $iPos = get_current_ap()
			If( $iPos > 1 ) Then $iSleep = 2
			Debug( "[SYS] Standort: " & $iPos )

			; Ziel bestimmen
			If( ($iPos = 1) And ($iSleep > 1) ) Then
				If( search_pattern( "sys/sys_flotte" ) ) Then
					Debug( "[SYS] Center" )
					mouse_move_relative( 0, 0, True )
					wait_sec( 1 )
				EndIf
				
				If( ((get_timestamp() - $iLastRepair) >= $_sys_repair_time) And search_pattern( "sys/sys_repair" ) ) Then
					Debug( "[SYS] Start Rep" )
					$iLastRepair = get_timestamp()

					mouse_move_relative( 0, 0, True )
					wait_sec( 0.5 )

					mouse_move_relative( 460, 125, True )
					$iSleep = 2
					wait_sec( 2 )
					$bDo = False
				EndIf
			EndIf

			If( $bDo ) Then
				$iWhich = -1
				Switch $iPos
					; Basis -----------------------------------------------------------
					Case 1
						$z = Random( 1, 100, 1 )
						If( $iSleep <= 1 ) Then
							$z = Random( 1, 2, 1 )
							If( $z = 1 ) Then $z = 1 ; Oben
							If( $z = 2 ) Then $z = 100 ; Unten
						EndIf

						Switch $z
							Case 1 To 10 ; Oben
								$iWhich = 8
								If( search_pattern( "common/symbol_settings" ) ) Then 
									mouse_move_relative( -105,	170, True ) ; Unten Links
									mouse_move_relative(		0, -110, True ) ; Oben Links: -105, 60
									wait_sec( 0.5 )
								EndIf
							Case 11 To 90 ; Mitte
								$iWhich = 9
							Case 91 To 100 ; Unten
								$iWhich = 7
								If( search_pattern( "common/symbol_settings" ) ) Then 
									mouse_move_relative( -105,	60, True ) ; Oben Links
									mouse_move_relative(		0, 110, True ) ; Unten Links: -105, 170
									wait_sec( 0.5 )
								EndIf
						EndSwitch
					; 1. Reihe --------------------------------------------------------
					Case 8
						$iWhich = 18
						If( search_pattern( "common/symbol_settings" ) ) Then
							mouse_move_relative(	-65, 140, True ) ; Unten Links/Mitte
							mouse_move_relative(		0, -60, True ) ; Oben Links/Mitte: -75, 80
							wait_sec( 0.5 )
						EndIf
					Case 9
						$z = Random( 1, 2, 1 )

						Switch $z
							Case 1
								$iWhich = 18
							Case 2
								$iWhich = 17
						EndSwitch
					Case 7
						$iWhich = 17
						If( search_pattern( "common/symbol_settings" ) ) Then
							mouse_move_relative(	-65,	90, True ) ; Oben Links/Mitte
							mouse_move_relative(		0,	60, True ) ; Unten Links/Mitte: -75, 150
							wait_sec( 0.5 )
						EndIf
					; 2. Reihe --------------------------------------------------------
					Case 11
						$iWhich = 21
					Case 18
						$z = Random( 1, 100, 1 )

						Switch $z
							Case 1 To 50
								$iWhich = 21
							Case 51 To 100
								$iWhich = 25
						EndSwitch
					Case 17
						$z = Random( 1, 100, 1 )

						Switch $z
							Case 1 To 50
								$iWhich = 25
							Case 51 To 100
								$iWhich = 20
						EndSwitch
					Case 10
						$iWhich = 20
					; 3. Reihe --------------------------------------------------------
					Case 22
						$iWhich = 13
					Case 21
						$iWhich = 24
					Case 25
						$z = Random( 1, 100, 1 )

						Switch $z
							Case 1 To 50
								$iWhich = 24
							Case 51 To 100
								$iWhich = 23
						EndSwitch
					Case 20
						$iWhich = 23
					Case 19
						$iWhich = 12
					; 4. Reihe --------------------------------------------------------
					Case 13
						$iWhich = 16
						If( search_pattern( "common/symbol_settings" ) ) Then 
							mouse_move_relative( -105,	60, True ) ; Links Oben
							mouse_move_relative(	95,	0, True ) ; Rechts Oben: -10, 60
							wait_sec( 0.5 )
						EndIf
					Case 24
						$z = Random( 1, 100, 1 )
						
						Switch $z
							Case 1 To 90
								$iWhich = 16
								If( search_pattern( "common/symbol_settings" ) ) Then 
									mouse_move_relative( -105,	60, True ) ; Links Oben
									mouse_move_relative(	95,	0, True ) ; Rechts Oben: -10, 60
									wait_sec( 0.5 )
								EndIf
							Case 91 To 100
								$iWhich = 14
						EndSwitch
					Case 23
						$z = Random( 1, 100, 1 )

						Switch $z
							Case 1 To 10
								$iWhich = 14
							Case 11 To 100
								$iWhich = 15
								If( search_pattern( "common/symbol_settings" ) ) Then 
									mouse_move_relative( -105, 170, True ) ; Links Unten
									mouse_move_relative(	95,	0, True ) ; Rechts Unten: -10, 170
									wait_sec( 0.5 )
								EndIf
						EndSwitch
					Case 12
						$iWhich = 15
						If( search_pattern( "common/symbol_settings" ) ) Then 
							mouse_move_relative( -105, 170, True ) ; Links Unten
							mouse_move_relative(	95,	0, True ) ; Rechts Unten: -10, 170
							wait_sec( 0.5 )
						EndIf
					; 5. Reihe --------------------------------------------------------
					Case 16
						$iWhich = -1 ; hier bleiben
					Case 14
						$iWhich = -1 ; hier bleiben
					Case 15
						$iWhich = -1 ; hier bleiben
				EndSwitch

				; Anfliegen
				If( ($iWhich >= 0) ) Then
					If( search_pattern( "sys/sys_ap_" & $iWhich, 90 ) ) Then
						Debug( "[SYS] Fliege -> " & $iWhich )

						mouse_move_relative( -40, -80, True )
						wait_sec( 0.5 )
						mouse_move_relative( -180, 0, True )
					Else
						Debug( "[ERROR]Planetoid " & $iWhich & " NICHT gefunden" )
						If( $iSleep > 1 ) Then $iLastPos = 0
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf

	If( (($hour = 7) Or ($hour = 13) Or ($hour = 20)) And ($min >= 40) And (Not $bInSys) ) Then $bStopSys = True
	If( (($hour = 7) Or ($hour = 13) Or ($hour = 20)) And ($min >= 41) ) Then $bStopSys = True
	If( ($hour <> 7) And ($hour <> 13) And ($hour <> 20) And ($hour <> 6) And ($hour <> 12) And ($hour <> 19) ) Then $bStopSys = True

	If( Not $bStopSys ) Then wait_sec( $iSleep	)
WEnd

reload_window()

Debug( "-----------------" )
Debug( "--- CSSS ENDE ---" )
Debug( "-----------------" )
