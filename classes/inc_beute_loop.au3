#include "base.au3"

Debug( "--------------------" )
Debug( "--- BEUTEMISSION ---" )
Debug( "--------------------" )

load_config_file()

$fight_type = 3 ; Farmen
$iLastPing = 0
$iMissionType = 0
$bBeuteFinished = False
$j = 0
$bInSektor = True
$iCounter = 0
$jj = 0
$iAnzAbschluss = 0
Global $iAnzAbschlussMission = 0
Global $iLastAbschluss = get_timestamp()

If( NOT $_standalone ) Then
	reset_mouse()
	If( $bInSektor ) Then
		switch_to_weapon_offi( $_beute_weapon1, $_beute_weapon2, $_beute_offi, $_beute_repair )
		If( $_beute_shuttles ) Then
			load_shuttles( $_beute_heal_shuttles )
		Else
			unload_shuttles()
		EndIf
		change_weapon_set( $_beute_weapon_set )
	EndIf
EndIf

While (Not $bBeuteFinished) And (Not event_starting_soon())
	$bOK = True

	If( check_dc() ) Then
		Debug( "[BM] DC..." )
		$bOK = False
	EndIf

	If( search_pattern( "common/flotte_fliegt" ) ) Then
		Debug( "[BM] Flotte fliegt" )
		$iLastPing = get_timestamp()
		$bOK = False
		$jj = 0
	EndIf

	If( search_pattern( "kampf/verlassen" ) ) Then
		Debug( "[ATT] Kampf verlassen Failsafe" )
		mouse_move_relative( 0, 0, true )
	EndIf
	
	Debug( "[BM] Anzahl: " & $iAnzAbschlussMission & "/" & $iAnzAbschluss & " - letzt " & (get_timestamp() - $iLastAbschluss) & " s" )

	If( (get_timestamp() - $iLastAbschluss) > 500 ) Then ; Irgenwo hängt es -> zu Sektor 1 und neu angfliegen
		Debug( "[BM] Timeout..." )
		; do_screenshot( "bm_timeout" )

		$bOK = False
		wait_sec( 3 )
		close_all_windows()

		If( search_pattern( "farm/suche" ) ) Then
			Debug( "[BM] Fliege Sektor 1" )

			search_pattern( "farm/suche" )
			; Koords einfügen
			mouse_move_relative( -30, 0, true )

			Send( "+{HOME}" )
			Send( "1:1:0" )

			; Klick Suchen
			mouse_move_relative( 30, 0, true )

			fly_to_sector()

			wait_sec( 5 )
			reload_window()

			; Weltkarte wechseln
			If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative( -180, 40, True )

			$iLastAbschluss = get_timestamp()
		Else
			reload_window()
		EndIf
	EndIf

	If( $bOK ) Then
		If( search_pattern( "kampf/att_start" ) ) Then
			Debug( "[BM] Kampf start" );
			do_fight( $fight_type )
		EndIf
	EndIf

	If( $bOK And (get_timestamp() > ($iLastPing + 5) )) Then
		; Neue Mission holen
		$ret = get_new_mission()
		Switch $ret
			Case 1
				; Neue Mission geholt
				$iCounter = 0
				$iAnzAbschluss = $iAnzAbschluss + 1
			Case -1
				; bereits aktiv
				$iCounter = 0
			Case -2
				; Keine Verfügbar
				$iCounter = $iCounter + 1
				If( $iCounter > 10 ) Then $bBeuteFinished = True
			Case -3
				; Abbruch
				$bBeuteFinished = True
			Case -4
				; Kann nicht abgeholt werden
				reload_window()
		EndSwitch

		Debug( "[BM] aktive Beutemission: " & $iMissionType )

		If( search_pattern( "beute/sek_angriff" ) ) Then
			mouse_move_relative( 0, 0, True )
			wait_sec( 1 )
		EndIf
		If( search_pattern( "beute/drohne" ) ) Then
			Debug( "[BM] Drohne klicken" )
			mouse_move_relative( 0, 0, True )
			wait_sec( 5 )
		EndIf
		If( search_pattern( "beute/detektor" ) ) Then
			Debug( "[BM] Detektor klicken" )
			mouse_move_relative( 0, 0, True )
			wait_sec( 5 )
		EndIf

		If( search_pattern( "farm/flotte_bewegen" ) ) Then mouse_move_relative( -70, 140, True )

		If( search_pattern( "beute/icon_beute_info" ) ) Then
			Debug( "[BM] Abschliessen -> Weiterfliegen" )
			mouse_move_relative( 0, 0, True )
			wait_sec( 1 )

			; Abschliessen
			If( search_pattern( "beute/abschliessen" ) ) Then
				mouse_move_relative( 0, 0, True )
				wait_sec( 1 )
				$iMissionType = 0
				$iLastAbschluss = get_timestamp()
				$iAnzAbschlussMission = $iAnzAbschlussMission + 1
				If( ($iAnzAbschlussMission > 0) And Mod( $iAnzAbschlussMission, 30 ) = 0 ) Then reload_window()
			EndIf

			; Mission starten
			If( search_pattern( "beute/begib_dich_zu_planetoid" ) ) Then ; nur Anfliegen
				mouse_move_relative( 80, 0, True )
				wait_sec( 1 )
				$iMissionType = 1
			EndIf
			If( search_pattern( "beute/nahe_der_position" ) ) Then
				mouse_move_relative( 60, 0, True )
				wait_sec( 1 )
				$iMissionType = 2
			EndIf
			If( search_pattern( "beute/reise_zu_system" ) ) Then ; Drohne klicken
				mouse_move_relative( 50, 0, True )
				wait_sec( 1 )
				$iMissionType = 3
			EndIf
			If( search_pattern( "beute/schliesse_ein_gefecht_bei" ) ) Then ; Angriff
				mouse_move_relative( 90, 0, True )
				wait_sec( 1 )
				$iMissionType = 4
			EndIf
			If( search_pattern( "beute/innerhalb_des_systems" ) ) Then ; 4 Kristalle sammeln
				mouse_move_relative( 80, 0, True )
				wait_sec( 1 )
				$iMissionType = 5
			EndIf
			If( search_pattern( "beute/drohne_auf_planetoid" ) ) Then ; Drohne klicken
				mouse_move_relative( 80, 0, True )
				wait_sec( 1 )
				$iMissionType = 6
			EndIf

			; Anfliegen
			If( search_pattern( "beute/sektorkarte" ) ) Then
				$j = 0
				fly_to_sector()
			Else
				close_all_windows()
				If( $iMissionType = 5 ) Then
					If( do_detail_mission( $iMissionType ) ) Then $iMissionType = 0
				EndIf

				Debug( "[BM] Keine Sektorenkarte: " & $j )
				$j = $j + 1
				If( $j > 3 ) Then
					Debug( "[BM] Failsafe" )

					$jj = $jj + 1
					If( $jj > 4 ) Then $jj = 0
					Switch $jj
						Case 1 ; Mittlerer Bereich
							$arr = get_array(	560, 330, 1340,	840 )
							$str = "Mitte";
						Case 2 ; Rechter Bereich
							$arr = get_array( 1300,	0, 1920, 1080 )
							$str = "Rechts";
						Case 3 ; Linker Bereich
							$arr = get_array(		0,	0,	600, 1080 )
							$str = "Links";
						Case 4 ; Center
							center_fleet()
							$arr = get_array(	560, 330, 1340,	840 )
							$str = "Center + Mitte";
					EndSwitch

					; Anfliegen suchen -> Mitte
					$bOK = False
					Debug( "[BM] Suche " & $str )
					If( (Not $bOK) And search_pattern( "beute/anfliegen_mitte", $arr, 100 ) ) Then
						Debug( "[BM] Fliege " & $str )
						mouse_move_relative( 0, 0, True )
						$bOK = True
					EndIf

					If( $bOK ) Then
						wait_sec( 0.5 )
						If( search_pattern( "farm/sektor_verlassen" ) ) Then ; Sektor verlassen Abfrage
						Debug( "[BM] Sektor verlassen" )
						mouse_move_relative( -70, 140, True )
						wait_sec( 5 )
						EndIf
					EndIf
				EndIf
			EndIf

			close_all_windows()
		EndIf
	EndIf

	wait_sec( 1 )
WEnd

If( search_pattern( "beute/icon_beute_info" ) ) Then
	mouse_move_relative( 0, 0, True )
	wait_sec( 1 )

	While( search_pattern( "beute/verwerfen" ) )
		mouse_move_relative( 0, 0, True )
		If( search_pattern( "beute/verwerfen_message" ) ) Then mouse_move_relative( -90, 140, True, True )
		wait_sec( 1 )
	WEnd
EndIf

Func fly_to_sector()
	Local $bOK = False
	Local $i = 0

	While( (Not $bOK) And ($i < 20) )
		Debug( "[SEK] Sektorenkarte -> Anfliegen" )
		$i = $i + 1

		If( search_pattern( "beute/pfeil", 0, 50 ) ) Then
			If( $_found_pattern = 1 ) Then
				mouse_move_relative( 0, 30, True, True )
			Else
				mouse_move_relative( 0, 20, True, True ) ; nur Spitze des Pfeils -> Failsafe (Image 2)
			EndIf

			Sleep( 500 )
			If( search_pattern( "beute/sek_anfliegen" ) ) Then
				mouse_move_relative( 0, 0, True )
				wait_sec( 1 )
				If( search_pattern( "farm/sektor_verlassen" ) ) Then ; Sektor verlassen Abfrage
					Debug( "[BM] Sektor verlassen" )
					mouse_move_relative( -70, 140, True )
					wait_sec( 5 )
				EndIf
				$bOK = True
			EndIf
		EndIf

		wait_sec( 1 )
	WEnd

	If( $i >= 20 ) Then close_all_windows()
EndFunc

Func do_detail_mission( $iMissionType )
	If( $iMissionType = 5 ) Then
		Debug( "[BM5] Sektor betreten" )
		center_fleet()

		$bInSektor = False
		$i = 0
		$k = 1

		While( (Not $bInSektor) And ($i <= 10 ) )
			$i = $i + 1

			Debug( "[BM5] Try..." )
			If( search_pattern( "beute/betreten" ) ) Then 
				mouse_move_relative( 0, 0, True )
;				$bInSektor = True
				wait_sec( 2 )
			EndIf
			If( search_pattern( "beute/wurmloch" ) ) Then $bInSektor = True
			If( search_pattern( "beute/sek_angriff" ) ) Then $bInSektor = True
			
			wait_sec( 0.5 )
		WEnd
		If( ($i >= 10) Or (Not $bInSektor) ) Then
			Debug( "[BM5] Kann Sektor nicht betreten" )

			Return( False )
		EndIf

		$bFinished = False
		While( Not $bFinished )
			If( check_dc() ) Then Debug( "[BM] DC..." )

			If( (get_timestamp() - $iLastAbschluss) > 500 ) Then $bFinished = True

			If( search_pattern( "kampf/verlassen" ) ) Then
				Debug( "[ATT] Kampf verlassen Failsafe" )
				mouse_move_relative( 0, 0, true )
			EndIf

			If( search_pattern( "kampf/att_start" ) ) Then
				Debug( "[BM5] Kampf start" );
				do_fight( 3 )
				wait_sec( 1 )
				center_fleet()
				wait_sec( 1 )
			EndIf

		If( Not $bFinished ) Then
			If( search_pattern( "beute/sek_angriff" ) ) Then
				mouse_move_relative( 0, 0, True, True )
				wait_sec( 1 )
				If( search_pattern( "farm/flotte_bewegen" ) ) Then
					Debug( "[BM] Flotte Bewegen" )
					mouse_move_relative( -70, 140, True )
				EndIf
				wait_sec( 5 )
			Else
				Debug( "[BM5] Kein Angriff gefunden" )

				If( search_pattern( "beute/icon_beute_info" ) ) Then
					mouse_move_relative( 0, 0, True )
					wait_sec( 1 )
					If( search_pattern( "beute/abschliessen" ) ) Then
						mouse_move_relative( 0, 0, True )
						$bFinished = True
						$iLastAbschluss = get_timestamp()
						$iAnzAbschlussMission = $iAnzAbschlussMission + 1
					EndIf
					close_all_windows()
				EndIf

				If( Not $bFinished ) Then
					center_fleet()
					wait_sec( 1 )

					$k = $k + 1
					If( $k > 13 ) Then $k = 1
					Debug( "[BM5] Positioniere neu: " & $k )
					Switch $k
						Case 1
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative( -105,	60, True ) ; Oben Links
						Case 2
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative( -105, 115, True ) ; Mitte Links
						Case 3
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative( -105, 170, True ) ; Unten Links
						Case 4
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative(	-75,	90, True ) ; Oben Links/Mitte
						Case 5
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative(	-75, 140, True ) ; Unten Links/Mitte
						Case 6
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative(	-55,	60, True ) ; Oben Mitte
						Case 7
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative(	-55, 115, True ) ; Mitte Mitte
						Case 8
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative(	-55, 170, True ) ; Unten Mitte
						Case 9
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative(	-10,	60, True ) ; Oben Rechts
						Case 10
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative(	-10, 115, True ) ; Mitte Rechts
						Case 11
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative(	-10, 170, True ) ; Unten Rechts
						Case 12
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative(	-30,	90, True ) ; Oben Rechts/Mitte
						Case 13
							If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative(	-30, 140, True ) ; Unten Rechts/Mitte
					EndSwitch
				EndIf
			EndIf
		EndIf

		wait_sec( 1 )
		WEnd

		If( $bFinished ) Then Return( True )
	EndIf

	Return( False )
EndFunc

move_to_base()
wait_sec( 2 )
reload_window()

Debug( "-------------------------" )
Debug( "--- BEUTEMISSION ENDE ---" )
Debug( "-------------------------" )


; geht nicht
; 3-11-1
; 4-4-3
; 8-5-1