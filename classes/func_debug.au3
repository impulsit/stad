#include-once

Global $_iWindowEditID
Global $iEdit[20]

Func write_debug_window( $str )
	Local $strLabel = ["Boss/Staffel","CSS","3v3","Ressourcen","Herausforderung","Wingman","Inter/Trials","Fragezeichen","Beutemission","Frachtmission"]

	If( Not $_show_debug_window ) Then Return

	Local $DebugHwnd = WinGetHandle( "[STAD]Debug" )
	If Not IsHwnd( $DebugHwnd ) Then
		GuiCreate ( "[STAD]Debug", 300, 1000, 0, 180, "", $WS_EX_TOPMOST )
		GuiSetState( @SW_SHOW )

		$_iWindowEditID = GUICtrlCreateEdit( "", 0, 110, 300, 862, $ES_AUTOVSCROLL + $WS_VSCROLL + $ES_READONLY )
 		GUICtrlSetFont( $_iWindowEditID, 8, 100, 0, "Consolas", 5 ) ;Courier New" )
		GUICtrlSetLimit( $_iWindowEditID, 9999 )
		GUICtrlSetState( $_iWindowEditID, $GUI_DISABLE )

		For $i = 0 To 4
			GUICtrlCreateLabel( $strLabel[$i] & ".......................", 2,	4+($i*18), 46, 16)
			$iEdit[$i] = GUICtrlCreateInput( "", 50, 2+($i*18), 92, 16, $ES_READONLY )
			GUICtrlSetState( $iEdit[$i], $GUI_DISABLE )
		Next
		For $i = 5 To 9
			GUICtrlCreateLabel( $strLabel[$i] & ".......................", 152,	4+(($i-5)*18), 46, 16)
			$iEdit[$i] = GUICtrlCreateInput( "", 200, 2+(($i-5)*18), 92, 16, $ES_READONLY )
			GUICtrlSetState( $iEdit[$i], $GUI_DISABLE )
		Next

		GUICtrlCreateLabel( "Hearthbeat.......................", 2,	4+(5*18), 46, 16)
		$iEdit[10] = GUICtrlCreateInput( "", 50, 2+(5*18), 242, 16, $ES_READONLY )
		GUICtrlSetState( $iEdit[10], $GUI_DISABLE )
	EndIf

	If( StringLen( GUICtrlRead( $_iWindowEditID, 0 ) ) > 9500 ) Then GUICtrlSetData( $_iWindowEditID, "" )

	GUICtrlSetData( $_iWindowEditID, $str, 1 )

	; Hearthbeat
	GUICtrlSetData( $iEdit[10], @HOUR & ":" & @MIN & ":" & @SEC )
EndFunc

Func write_debug_window_data( $i, $value )
	GUICtrlSetData( $iEdit[$i], $value )

	; Hearthbeat
	GUICtrlSetData( $iEdit[10], @HOUR & ":" & @MIN & ":" & @SEC )
EndFunc
