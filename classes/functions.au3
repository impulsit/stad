#include-once
#include "func_coords.au3"
#include "func_ini_file.au3"
#include "func_debug.au3"

Func write_file( $str )
	$strFilename = "logs/" & @YEAR & "-" & @MON & "-" & @MDAY & ".txt"

	$hFile = FileOpen( $strFilename, $FO_APPEND )
	If( $hFile <> -1 ) Then
		FileWriteLine( $hFile, $str )
		FileClose( $hFile )
	EndIf
EndFunc

Global $_strLastDebugText
Global $_iPointCounter
Func debug( $str )
	If( $str <> "." ) Then
		$_iPointCounter = 0
		If( $_strLastDebugText = "." ) Then ConsoleWrite( @CRLF )

		$str = "[" & @HOUR & ":" & @MIN & ":" & @SEC & "] " & $str & @CRLF
		ConsoleWrite( $str )
		write_file( $str )
		write_debug_window( $str )
	Else
		ConsoleWrite( $str )
		$_iPointCounter = $_iPointCounter + 1
		If( $_iPointCounter > 80 ) Then
			$_iPointCounter = 0
			ConsoleWrite( @CRLF )
		EndIf
	EndIf

	$_strLastDebugText = $str
EndFunc

Global $_found_pattern = 0
Func search_pattern( $img_path_orig, $type = 0, $tolerance = 80 )
	$_x = 0
	$_y = 0

	Local $i = 0
	$bFoundL = False
	Do
		$i = $i + 1
		If( $i = 1 ) Then
			$img_path = "img/" & $img_path_orig & ".bmp"
		Else
			$img_path = "img/" & $img_path_orig & "_" & $i & ".bmp"
		EndIf

		If (Not $bFoundL) And (FileExists( $img_path ) <> 0) Then
			If( Not IsArray( $type ) ) Then
				$arr = 0
				If( get_array_for_pattern( "img/" & $img_path_orig & ".bmp", $arr ) ) Then $type = $arr
			EndIf

			$start = get_timestamp()
			Switch $img_path_orig
				Case "common/flotte_fliegt":
					$tolerance = 50
			EndSwitch
			
			$iResult = _ImageSearch( $img_path, 1, $_x, $_y, $tolerance, $type )
			$zeit = (get_timestamp() - $start)

			If( $iResult = 1 ) Then $bFoundL = True
		EndIf
	Until $bFoundL Or ($i > 10)

	$_found_pattern = $i

	Return( $bFoundL )
EndFunc

Func search_pattern_repeat( $img_path )
	$i = 0
	$bEnd = False
	Do
		$i = $i + 1
		If( search_pattern( $img_path ) ) Then $bEnd = True
	Until $bEnd Or ($i > 10)

	Return( $bEnd )
EndFunc

Func mouse_move_relative( $iX, $iY, $bClick, $bMoveRandom = False )
	$_x = $_x + $iX
	$_y = $_y + $iY

	If( $bMoveRandom ) Then
		$_x = $_x + Random( 0, 20, 1 ) - 10
		$_y = $_y + Random( 0, 20, 1 ) - 10
	EndIf

	; Save Mouse Pos
	$arrPos = MouseGetPos()

	MouseMove( $_x, $_y, 0 )
	If( $bClick ) Then MouseClick( "left" )

	; Restore Mouse Pos
	MouseMove( $arrPos[0], $arrPos[1], 0 )

	If( $bClick ) Then Sleep( 250 )
EndFunc

Func mouse_move_relative_debug( $iX, $iY, $bNothing = False )
	$_x = $_x + $iX
	$_y = $_y + $iY

	MouseMove( $_x, $_y, 10 )
EndFunc

Func fly_to_coord( $str )
	$_x = 0
	$_y = 0
	$bOK = false

	If( search_pattern( "farm/suche" ) ) Then
		; Koords einfügen
		mouse_move_relative( -30, 0, true )

		Send( "+{HOME}" )
		Send( $str )

		; Klick Suchen
		mouse_move_relative( 30, 0, true )

		If( search_pattern_repeat( "farm/map_pfeil" ) ) Then
			; Klick unterhalb von Pfeil
			mouse_move_relative( 0, 30, True )
			Sleep( 500 )
			mouse_move_relative( -115, 20, True )
			$bOK = True
		EndIf
	EndIf

	Return( $bOK )
EndFunc

Func make_coord( $iSek, $iPos, $iDetail )
	$str = $iSek & ":" & $iPos & ":" & $iDetail

	Return( $str )
EndFunc

Func do_fight( $type, $stage = 0 )
	; 1 = Boss, 2 = Herausforderung, 3 = Farmen, 4 = Plündern, 5 = Shuttle, 6 = Interstellar
	Debug( "[ATT] Start Kampf" )

	$_InFight = True
	$last_skill_time = get_timestamp()
	$start_time = get_timestamp()
	$bFailed = False

	While $_InFight
		For $i = 0 To ($_max_skills-1)
			If( search_pattern( "kampf/" & $_skill[$i][0] ) ) Then
				Debug( "[ATT] (" & (get_timestamp() - $start_time) & ") Klick " & $_skill[$i][0] );
				mouse_move_relative( 0, 0, True )
				mouse_move_relative( 0, -50, False )
				$last_skill_time = get_timestamp()
			EndIf
		Next

		; Ende + Weiter für Herausforderung
		If( $type = 2 ) Then
			If( search_pattern( "heraus/weiter" ) ) Then
				Debug( "[ATT]Weiter" );
				mouse_move_relative( 0, 0, true )
			EndIf

			If( search_pattern( "kampf/verlassen" ) ) Then
				Debug( "[ATT]Verlassen" )
				If( search_pattern( "heraus/her_failed" ) ) Then
					Debug( "[ATT]Besiegt" );
					$bFailed = True
				EndIf

				If( search_pattern( "kampf/verlassen" ) ) Then
					mouse_move_relative( 0, 0, true )
					$_InFight = False
				EndIf
			EndIf
		EndIf

		; Ende für Farmen + Interstellar + Shuttle
		If( ($type = 3) Or ($type = 5) Or ($type = 6) ) Then
			If( search_pattern( "kampf/verlassen" ) ) Then
				Debug( "[ATT]Verlassen" )
				mouse_move_relative( 0, 0, true )
				$_InFight = False
			EndIf
			If( search_pattern( "shuttle/shu_restart" ) ) Then
				Debug( "[ATT]Verlassen" )
				mouse_move_relative( 20, 135, True )
				$bFailed = True
			EndIf
		EndIf

		; Ende für Plündern
		If( ($type = 4) ) Then
			If( search_pattern( "kampf/schliessen", 3 ) ) Then
				Debug( "[ATT]Schliessen" );
				mouse_move_relative( 0, 0, true )
				$_InFight = False
			EndIf
		EndIf

		; Kein Skill seit X Sekunden
		If( (get_timestamp() - $last_skill_time) > 25 ) Then
			Debug( "[ATT]Abbruch..." );
			$_InFight = False
		EndIf
		; dauert zu lange
		If( (get_timestamp() - $start_time) > 200 ) Then
			Debug( "[ATT]Abbruch..." );
			$_InFight = False
		EndIf

		wait_sec( 0.1 )
	WEnd

	Return( $bFailed )
EndFunc

Func get_timestamp()
	$iDateCalc = _DateDiff('s', "1970/01/01 00:00:00", _NowCalc())

	Return( $iDateCalc )
EndFunc

Func send_key( $str )
	WinActivate ( "Star Trek"	)

	Send( $str )
EndFunc

Func check_dc( $bReload = True )
	; 5 Uhr früh Lag
	If( (Int( @HOUR ) = 4) And (Int( @MIN ) >= 57) ) Then pause( 12 )

	If WinExists( "Gesponserte" ) Then
		Debug( "[DC] Close Teamviewer" )
		WinSetState( "Gesponserte", "", @SW_SHOW )
		WinActivate( "Gesponserte" )
		Send( "{ENTER}" )
	EndIf
	If WinExists( "Verbindungs" ) Then
		Debug( "[DC] Close Teamviewer" )
		WinSetState( "Verbindungs", "", @SW_SHOW )
		WinActivate( "Verbindungs" )
		Send( "{ENTER}" )
	EndIf

	If( search_pattern( "common/check_dc" ) ) Then
		Debug( "[DC] Reload 1" )
		reload_window()

		Return( True )
	EndIf

	If( search_pattern( "common/false_conn" ) ) Then
		Debug( "[DC] Reload 2" )
		reload_window()

		Return( True )
	EndIf

	If( search_pattern( "common/no_inet" ) ) Then
		Debug( "[DC] Kein Internet" )

		Debug( "[DC] Reload 5" )
		reload_window()

		Return( True )
	EndIf

	If( search_pattern( "common/shockwave_crashed" ) ) Then
		Debug( "[DC] Flash Crashed" )
		do_screenshot( "crash" )

		Debug( "[DC] Reload 6" )
		reload_window()

		Return( True )
	EndIf

	If( search_pattern( "common/chrome_crash" ) ) Then
		Debug( "[DC] Chrome Crashed" )
		do_screenshot( "crash" )

		mouse_move_relative( 0, 0, True )
		Sleep( 5 )
		start_browser()

		Return( True )
	EndIf

	If( search_pattern( "common/load_error" ) ) Then
		Debug( "[DC] Load Error" )
		do_screenshot( "crash" )

		reload_window()

		Return( True )
	EndIf
	
	start_browser()

	Return( False )
EndFunc

Func reload_window()
	global $_disable_reload_window
	
	If( $_disable_reload_window ) Then
		Debug( "[RELOAD] --- KEIN Reload Window ---" )
		Return
	EndIf
	
	Debug( "[RELOAD] --- Reload Window ---" )

	If( (WinActivate ( "Star Trek"	) > 0) Or (WinActivate ( "startrek"	) > 0) ) Then
		MouseMove( Round( @DesktopWidth / 2) - 200, 175, 2 )
		MouseClick( "left" );

		Send( "^{F5}" )
		Debug( "[RELOAD] Sende CTRL+F5" )

		wait_sec( 1 )

		$i = 0
		$bReloaded = False
		While search_pattern( "common/reload", 0 ) And ($i < 10)
			$i = $i + 1

			If( search_pattern( "common/reload", 0 ) ) Then
				Debug( "[RELOAD] Klick Reload" )
				mouse_move_relative( 0, 0, True, True )
				$bReloaded = True
			EndIf
		Wend

		If( $bReloaded ) Then
			wait_for_game_ready()

			Debug( "[RELOAD] --- Reload Window ENDE ---" )

			Return( True )
		EndIf
	EndIf

	Debug( "[RELOAD] Chrome Hardkill" )
	While( ProcessExists( "chrome.exe" ) )
		$iPid = ProcessExists( "chrome.exe" )
		ProcessClose( $iPid )
	Wend
	start_browser()

	Debug( "[RELOAD] --- Reload Window ENDE ---" )

	Return( True )
EndFunc

Func drag( $delta_x, $delta_y, $off_x = 0, $off_y = 0 )
	If( ($delta_x = 0) And ($delta_y = 0) ) Then Return

	If( ($off_x = 0) And ($off_y = 0) ) Then
		$x_offset = Random( 0, 200 ) - 100
		$y_offset = Random( 0, 200 )
	Else
		$x_offset = $off_x
		$y_offset = $off_y
	EndIf

	$center_x = Round( @DesktopWidth / 2, 0 ) + $x_offset
	$center_y = Round( (@DesktopHeight - 1080) / 2, 0 ) + 500 + $y_offset

	AutoItSetOption( "MouseClickDragDelay", 500 )
	MouseClickDrag( $MOUSE_CLICK_LEFT, $center_x, $center_y, $center_x + $delta_x, $center_y - $delta_y, 1 )
EndFunc

Func drag_from_to( $x, $y, $delta_x, $delta_y )
	$oX = Round( (@DesktopWidth - 1920) / 2, 0 )
	$oY = Round( (@DesktopHeight - 1080) / 2, 0 )

	AutoItSetOption( "MouseClickDragDelay", 500 )
	MouseClickDrag( $MOUSE_CLICK_LEFT, $x, $y, $oX + $delta_x, $oY + $delta_y, 1 )
EndFunc

Func wait_sec( $iTime, $iTo = 0 )
	If( $iTo > 0 ) Then $iTime = Random( $iTime, $iTo, 1 )
	If( $iTime >= 5 ) Then Debug( "[MAIN] Warte " & $iTime & "s ..." )
	If( ($iTime > 1) And ($iTime < 5) ) Then Debug( "." )

	Sleep( $iTime * 1000 )
EndFunc

Func wait_min( $iTime, $iTo = 0 )
	wait_sec( $iTime * 60, $iTo * 60 )
EndFunc

Func collect_res()
	Debug( "[RES] --- Ressourcen einsammeln ---" )

	close_all_windows()

	For $i = 1 To 5
		If( search_pattern( "res/base_details" ) ) Then
			If( $i = 1 ) Then
				Debug( "[RES] sammeln bei Basis" )
			Else
				Debug( "[RES] sammeln bei Kolo " & ($i - 1) )
			EndIf

			If( $i = 1 ) Then mouse_move_relative( 60, -40, True )
			If( $i = 2 ) Then mouse_move_relative( -10, -50, True )
			If( $i = 3 ) Then mouse_move_relative( 10, -90, True )
			If( $i = 4 ) Then mouse_move_relative( 50, -115, True )
			If( $i = 5 ) Then mouse_move_relative( 100, -100, True )
			wait_sec( 2 )

			If( search_pattern( "res/res_syn", 0 ) ) Then mouse_move_relative( 0, -110, True )
			If( search_pattern( "res/res_deut", 0 ) ) Then mouse_move_relative( 0, -110, True )
			If( search_pattern( "res/res_met", 0 ) ) Then mouse_move_relative( 20, -100, True )
			If( search_pattern( "res/res_kryst", 0 ) ) Then mouse_move_relative( 20, -100, True )
			wait_sec( 5 )
		EndIf
	Next

	close_all_windows()

	center_fleet()

	Debug( "[RES] --- Ressourcen einsammeln ENDE ---" )
EndFunc

Func get_array( $a1, $a2, $a3, $a4 )
  ; Chrome Fix
	$a4 = $a4 + 10

	Local $arr = [$a1, $a2, $a3, $a4]
	Return $arr
EndFunc

Func switch_to_weapon( $weapon1, $weapon2 )
	Debug( "[MAIN] --- Change Weapon ---" )

	close_all_windows()

	If( Not search_pattern( "common/icon_flotte" ) ) Then Return

	If( wait_for_window( "common/icon_flotte", "common/icon_flag_settings" ) ) Then
		If( wait_for_window( "common/icon_flag_settings", "common/schiffsdetails" ) ) Then
			; Waffe 1
			If( search_pattern( "common/schiffsdetails" ) ) Then
				mouse_move_relative( -315, 130, True )
				wait_sec( 0.5 )
				If( search_pattern( "common/ausruestung" ) ) Then mouse_move_relative( -45, 0, True )
				If( search_pattern( "common/" & $weapon1 ) ) Then mouse_move_relative( 0, 10, True )

				; Rechtes Fenster schliessen
				If( search_pattern( "common/button_close", get_array( 1460, 310, 1560, 400 ) ) ) Then mouse_move_relative( 0, 00, True )
			EndIf
			; Waffe 2
			If( search_pattern( "common/schiffsdetails" ) ) Then
				mouse_move_relative( -315, 245, True )
				wait_sec( 0.5 )
				If( search_pattern( "common/ausruestung" ) ) Then mouse_move_relative( -45, 0, True )
				If( search_pattern( "common/" & $weapon2 ) ) Then mouse_move_relative( 0, 10, True )

				; Rechtes Fenster schliessen
				If( search_pattern( "common/button_close", get_array( 1460, 310, 1560, 400 ) ) ) Then mouse_move_relative( 0, 00, True )
			EndIf
		EndIf
	EndIf

	close_all_windows()

	Debug( "[MAIN] --- Change Weapon ENDE ---" )
EndFunc

Func switch_to_weapon_offi( $weapon1, $weapon2, $strWhich, $bRepair = false )
	Debug( "[MAIN] --- Change Weapon & Offi ---" )

	close_all_windows()

	If( Not search_pattern( "common/icon_flotte" ) ) Then Return

	If( wait_for_window( "common/icon_flotte", "common/icon_flag_settings" ) ) Then
		If( wait_for_window( "common/icon_flag_settings", "common/schiffsdetails" ) ) Then
			; Waffe 1
			Debug( "[MAIN] Weapon 1: " & $weapon1 )
			If( search_pattern( "common/schiffsdetails" ) ) Then
				mouse_move_relative( -315, 130, True )
				wait_sec( 0.5 )
				If( search_pattern( "common/ausruestung" ) ) Then 
					mouse_move_relative( -45, 0, True )
					wait_sec( 0.5 )
				EndIf
				If( search_pattern( "common/" & $weapon1 ) ) Then mouse_move_relative( 0, 10, True )

				; Rechtes Fenster schliessen
				If( search_pattern( "common/button_close", get_array( 1460, 310, 1560, 400 ) ) ) Then mouse_move_relative( 0, 00, True )
			EndIf

			; Waffe 2
			Debug( "[MAIN] Weapon 2: " & $weapon2 )
			If( search_pattern( "common/schiffsdetails" ) ) Then
				mouse_move_relative( -315, 245, True )
				wait_sec( 0.5 )
				If( search_pattern( "common/ausruestung" ) ) Then 
					mouse_move_relative( -45, 0, True )
					wait_sec( 0.5 )
				EndIf
				If( search_pattern( "common/" & $weapon2 ) ) Then mouse_move_relative( 0, 10, True )

				; Rechtes Fenster schliessen
				If( search_pattern( "common/button_close", get_array( 1460, 310, 1560, 400 ) ) ) Then mouse_move_relative( 0, 00, True )
			EndIf

			; Offi
			Debug( "[MAIN] Offi: " & $strWhich )
			If( Not search_pattern( "offi/" & $strWhich & "_big" ) ) Then
				search_pattern( "common/schiffsdetails" )
				mouse_move_relative( -180, 100, True )
				wait_sec( 0.5 )
				If( search_pattern( "offi/befehligt" ) ) Then mouse_move_relative( 0, 0, True )

				$i = 0
				$bFound = False
				While( (Not $bFound) And ($i < 7) )
					$i = $i + 1

					If( search_pattern( "offi/" & $strWhich ) ) Then
						mouse_move_relative( 0, 0, True )
						$bFound = True
					EndIf
					If( Not $bFound ) Then
						If( search_pattern( "offi/_down" ) ) Then
							MouseMove( $_x, $_y, 0 )
							For $j = 1 To 15
								MouseClick( "left" )
								Sleep( 10 )
							Next
						EndIf
					EndIf
					wait_sec( 2 )
				WEnd
			EndIf

			; Repair
			If( $bRepair ) Then
				Debug( "[MAIN] Repair" )

				$bRepFinished = False
				$i = 0
				While (Not $bRepFinished) And ($i < 20)
					$i = $i + 1

					If( search_pattern( "common/flotte_rep" ) ) Then
						mouse_move_relative( 0, 0, True )
						wait_sec( 0.5 )

						If( search_pattern( "common/rep_finished" ) ) Then 
							$bRepFinished = True
							Debug( "[MAIN] Repair finished" )
						EndIf
						If( search_pattern( "common/rep_kit_1" ) ) Then
							mouse_move_relative( -420, -275, True )
							mouse_move_relative( 420, 275, True )
							mouse_move_relative( 170, 80, True )
							mouse_move_relative( 0, 0, True )
							mouse_move_relative( 0, 0, True )
							wait_sec( 1 )
						EndIf
						If( search_pattern( "common/button_close" ) ) Then mouse_move_relative( 0, 0, True )
						wait_sec( 1 )
					EndIf
				WEnd

			EndIf
		EndIf
	EndIf

	close_all_windows()

	Debug( "[MAIN] --- Change Weapon & Offi ENDE ---" )
EndFunc

Func reset_mouse()
	; DC
	check_dc()

	; offene Fenster schliessen
	close_all_windows()

	; Zentrieren
	If( search_pattern( "sys/sys_flotte", 0 ) ) Then mouse_move_relative( 0, 0, True )

	; Mouse Reset links oben
	MouseMove( Round( (@DesktopWidth - 1920) / 2 ) + 10, Round( (@DesktopHeight - 1080 ) / 2 ) + 110, 2 )
	MouseClick( "left" );
EndFunc

Func repair_fleet()
	Debug( "[MAIN] --- Repair Fleet ---" )

	$bInSys = False
	If( search_pattern( "sys/sys_start_into" ) ) Then
		Debug( "[MAIN] Sys Repair" )
		$bInSys = True
	EndIf

	close_all_windows()

	If( Not search_pattern( "common/icon_flotte" ) ) Then Return

	If( wait_for_window( "common/icon_flotte", "common/icon_flag_settings" ) ) Then
		If( wait_for_window( "common/icon_flag_settings", "common/schiffsdetails" ) ) Then
			$bRepFinished = False
			$i = 0
			While (Not $bRepFinished) And ($i < 20)
				$i = $i + 1

				If( search_pattern( "common/flotte_rep" ) ) Then
					mouse_move_relative( 0, 0, True )
					wait_sec( 0.5 )

					If( search_pattern( "common/rep_finished" ) ) Then $bRepFinished = True
					If( Not $bInSys ) Then
						If( search_pattern( "common/rep_kit_1" ) ) Then
							mouse_move_relative( -420, -275, True )
							mouse_move_relative( 420, 275, True )
							mouse_move_relative( 170, 80, True )
							mouse_move_relative( 0, 0, True )
							mouse_move_relative( 0, 0, True )
							wait_sec( 1 )
						EndIf
						Else
						If( search_pattern( "common/rep_kit_1_sys" ) ) Then
							mouse_move_relative( -310, -185, True )
							mouse_move_relative( 310, 185, True )
							mouse_move_relative( 130, 95, True )
							mouse_move_relative( 0, 0, True )
							mouse_move_relative( 0, 0, True )
							wait_sec( 1 )
						EndIf
					EndIf
					If( search_pattern( "common/button_close" ) ) Then mouse_move_relative( 0, 0, True )
				EndIf
			WEnd
		EndIf
	EndIf

	close_all_windows()

	Debug( "[MAIN] --- Repair Fleet ENDE ---" )
EndFunc

Func wait_for_window( $image_for_open, $image_if_opened )
	$bWindowOpen = False
	$bKlickFound = False
	$iStarttime = get_timestamp()
	While (Not $bWindowOpen) And ((get_timestamp() - $iStarttime) < 15)
		If( search_pattern( $image_if_opened ) ) Then $bWindowOpen = True
		If( (Not $bWindowOpen) And search_pattern( $image_for_open ) ) Then
			mouse_move_relative( 0, 0, True, $bKlickFound ) ; 2. Mal Random Klick
			$bKlickFound = True
			wait_sec( 0.5 )
		EndIf

		wait_sec( 0.1 )
	WEnd

	If( Not $bWindowOpen ) Then
		Debug( "[MAIN] Warten erfolglos: " & $image_if_opened )
		close_all_windows()
		Return( False )
	EndIf

	Return( True )
EndFunc

Func load_shuttles( $bHealFlag = True )
	Debug( "[MAIN] --- Load Shuttles ---" )

	close_all_windows()

	If( Not search_pattern( "shuttle/icon_shuttle" ) ) Then Return
	If( Not IsDeclared( "bHealFlag" ) ) Then $bHealFlag = True

	If( wait_for_window( "shuttle/icon_shuttle", "shuttle/ausstattung" ) ) Then
		If( wait_for_window( "shuttle/ausstattung", "shuttle/wingman_ausstattung_text" ) ) Then
		Local $arrShuttle[10]
		$arrShuttle[1] = "alpha"
		$arrShuttle[2] = "beta"
		$arrShuttle[3] = "gamma"
		$arrShuttle[4] = "zeta"
		$arrShuttle[5] = "nu"
		$arrShuttle[6] = "theta"
		$arrShuttle[7] = "kappa"
		$arrShuttle[8] = "delta"
		$arrShuttle[9] = ""
		Local $arrShip[10]
		If( $bHealFlag ) Then ; PvP
			$arrShip[1] = ""
			$arrShip[2] = "frost"
			$arrShip[3] = "expedition"
			$arrShip[4] = "razor"
			$arrShip[5] = "star_chaser"
			$arrShip[6] = ""
			$arrShip[7] = ""
			$arrShip[8] = ""
			$arrShip[9] = ""
		Else ; Boss
			$arrShip[1] = ""
			$arrShip[2] = "frost"
			$arrShip[3] = "razor"
			$arrShip[4] = "star_chaser"
			$arrShip[5] = "expedition"
			$arrShip[6] = ""
			$arrShip[7] = ""
			$arrShip[8] = ""
			$arrShip[9] = ""
		EndIf

		For $j = 1 To 5
			If( search_pattern( "shuttle/repair" ) ) Then mouse_move_relative( 0, 0, True )
			If( search_pattern( "shuttle/wingman_ausstattung_text" ) ) Then mouse_move_relative( -65, 400, True )

			If( $j = 1 ) Then ; unload old
				wait_sec( 1 )
				$i = 0
				While( search_pattern( "shuttle/small_shuttle_icon" ) And ($i < 10) )
					mouse_move_relative( 50, -5, True )
					$i = $i + 1
				WEnd
			EndIf

			For $i = 1 To 5
				If( search_pattern( "shuttle/shuttle_" & $arrShuttle[$i] & "_big" ) And ($arrShip[$i] <> "") ) Then
					Debug( "found: " & $arrShuttle[$i] )
					wait_for_window( "shuttle/ship_" & $arrShip[$i], "shuttle/ship_" & $arrShip[$i] )
					If( search_pattern( "shuttle/ship_" & $arrShip[$i] ) ) Then mouse_move_relative( 0, 0, True )
					Debug( $arrShuttle[$i] & " --> " & $arrShip[$i] )
					$i = 6
				EndIf
			Next

			If( search_pattern( "shuttle/next" ) ) Then mouse_move_relative( 0, 0, True )
			wait_sec( 0.5 )
		Next
		EndIf
	EndIf

	close_all_windows()

	Debug( "[MAIN] --- Load Shuttles ENDE ---" )
EndFunc

Func unload_shuttles()
	Debug( "[MAIN] --- Unload Shuttles ---" )

	close_all_windows()

	If( Not search_pattern( "shuttle/icon_shuttle" ) ) Then Return

	If( wait_for_window( "shuttle/icon_shuttle", "shuttle/ausstattung" ) ) Then
		If( wait_for_window( "shuttle/ausstattung", "shuttle/wingman_ausstattung_text" ) ) Then
			If( search_pattern( "shuttle/wingman_ausstattung_text" ) ) Then
				mouse_move_relative( -65, 400, True )
				wait_sec( 1 )

				$i = 0
				While( search_pattern( "shuttle/small_shuttle_icon" ) And ($i < 10) )
					mouse_move_relative( 50, -5, True )
					$i = $i + 1
				WEnd
			EndIf
		EndIf
	EndIf

	close_all_windows()

	Debug( "[MAIN] --- Unload Shuttles ---" )
EndFunc

Func close_all_windows()
	Local $i = 0

	; Boss aufgeben
	If( search_pattern( "boss/boss_aufgeben" ) ) Then
		mouse_move_relative( 0, 0, True )
		If( search_pattern( "boss/boss_aufgeben_text" ) ) Then mouse_move_relative( 10, 135, True )
	EndIf

	; Kampf verlassen
	$i = 0
	While( search_pattern( "kampf/verlassen" ) And ($i < 20) )
		mouse_move_relative( 0, 0, True, True )
		$i = $i + 1
	WEnd

	; X Klicken
	$i = 0
	While( search_pattern( "common/button_close" ) And ($i < 20) )
		mouse_move_relative( 0, 0, True, True )
		$i = $i + 1
	WEnd

	; Info Nachrichten wegklicken
	If( search_pattern( "common/icon_info_message" ) ) Then mouse_move_relative( 22, -17, True )

	; Post Nachrichten wegklicken
	If( search_pattern( "common/icon_email_message" ) ) Then mouse_move_relative( 22, -17, True )
	
	; Chrome Crash Close
	If( search_pattern( "common/chrome_crash_close" ) ) Then mouse_move_relative( 0, 0, True )
EndFunc

Func get_koords( $iType = 0 )
	$x_base = 0
	$y_base = 0

	If Not IsDeclared( "iType" ) Then $iType = 1

	While ($x_base = 0)
		Debug( "[SYS]Map Koordinaten bestimmen" )
		If( search_pattern( "common/symbol_settings" ) ) Then
			$x_base = $_x - 152
			$y_base = $_y + 17
		EndIf
	WEnd

	If( search_pattern( "common/mini_map_location", 0, 90 ) ) Then
		$x_offset = $_x - $x_base
		$y_offset = $_y - $y_base

		If( $iType = 0 ) Then
			Debug( "Offset found: " & $x_offset & " / " & $y_offset & " AP -> " & get_ap_no( $x_offset, $y_offset ) )
		Else
			Debug( "Offset found: " & $x_offset & " / " & $y_offset & " AP -> " & get_sys_no( $x_offset, $y_offset ) )
		EndIf
	Else
		Debug( "No Map Location found" )
	EndIf
EndFunc

Func get_current_ap()
	$x_base = 0
	$y_base = 0

	$i = 0
	While ($x_base = 0) And ($i < 20)
		$i = $i + 1
		If( search_pattern( "common/symbol_settings" ) ) Then
			$x_base = $_x - 152
			$y_base = $_y + 17
		EndIf
	WEnd

	If( search_pattern( "common/mini_map_location", 0, 90 ) ) Then
		$x_offset = $_x - $x_base
		$y_offset = $_y - $y_base

		Return( get_ap_no( $x_offset, $y_offset ) )
	EndIf

	Return( -1 )
EndFunc

Func get_current_sys()
	$x_base = 0
	$y_base = 0

	$i = 0
	While ($x_base = 0) And ($i < 20)
		$i = $i + 1
		If( search_pattern( "common/symbol_settings" ) ) Then
			$x_base = $_x - 152
			$y_base = $_y + 17
		EndIf
	WEnd

	If( search_pattern( "common/mini_map_location", 0, 90 ) ) Then
		$x_offset = $_x - $x_base
		$y_offset = $_y - $y_base

		Return( get_sys_no( $x_offset, $y_offset ) )
	EndIf

	Return( -1 )
EndFunc

Func get_ap_no( $x, $y )
	Local $k[30][3]

	For $i = 1 To 25
		$k[$i][1] = 0
		$k[$i][2] = 0
	Next

	$k[ 1][1] = 17
	$k[ 1][2] = 90

	$k[ 8][1] = 36
	$k[ 8][2] = 51
	$k[ 9][1] = 56
	$k[ 9][2] = 90
	$k[ 7][1] = 36
	$k[ 7][2] = 128

	$k[11][1] = 76
	$k[11][2] = 32
	$k[18][1] = 78
	$k[18][2] = 70
	$k[17][1] = 76
	$k[17][2] = 109
	$k[10][1] = 76
	$k[10][2] = 147

	$k[22][1] = 96
	$k[22][2] = 13
	$k[21][1] = 96
	$k[21][2] = 51
	$k[25][1] = 98
	$k[25][2] = 90
	$k[20][1] = 98
	$k[20][2] = 128
	$k[19][1] = 96
	$k[19][2] = 166

	$k[13][1] = 116
	$k[13][2] = 32
	$k[24][1] = 116
	$k[24][2] = 70
	$k[23][1] = 116
	$k[23][2] = 109
	$k[12][1] = 116
	$k[12][2] = 147

	$k[16][1] = 155
	$k[16][2] = 51
	$k[14][1] = 135
	$k[14][2] = 90
	$k[15][1] = 155
	$k[15][2] = 128

	$x_diff = 5
	$y_diff = 5

	$iFound = -1
	$i = 0
	While( ($iFound = -1) And ($i <= 25) )
		$i = $i + 1
		If( (($x - $x_diff) < $k[$i][1]) And (($x + $x_diff) > $k[$i][1]) And (($y - $y_diff) < $k[$i][2]) And (($y + $y_diff) > $k[$i][2]) ) Then $iFound = $i
	WEnd

	Return( $iFound )
EndFunc

Func get_sys_no( $x, $y )
	Local $k[30][3]

	For $i = 1 To 25
		$k[$i][1] = 0
		$k[$i][2] = 0
	Next

	$k[ 1][1] = 36
	$k[ 1][2] = 33
	$k[ 2][1] = 26
	$k[ 2][2] = 97
	$k[ 3][1] = 40
	$k[ 3][2] = 138
	$k[ 4][1] = 47
	$k[ 4][2] = 76
	$k[ 5][1] = 62
	$k[ 5][2] = 103
	$k[ 6][1] = 76
	$k[ 6][2] = 23
	$k[ 7][1] = 86
	$k[ 7][2] = 60
	$k[ 8][1] = 86
	$k[ 8][2] = 130
	$k[ 9][1] = 109
	$k[ 9][2] = 85
	$k[10][1] = 126
	$k[10][2] = 38
	$k[11][1] = 132
	$k[11][2] = 69
	$k[12][1] = 127
	$k[12][2] = 125
	$k[13][1] = 155
	$k[13][2] = 28
	$k[14][1] = 160
	$k[14][2] = 75
	$k[15][1] = 159
	$k[15][2] = 130
	$k[16][1] = 50
	$k[16][2] = 40

	$x_diff = 5
	$y_diff = 5

	$iFound = -1
	$i = 0
	While( ($iFound = -1) And ($i <= 16) )
		$i = $i + 1
		If( (($x - $x_diff) < $k[$i][1]) And (($x + $x_diff) > $k[$i][1]) And (($y - $y_diff) < $k[$i][2]) And (($y + $y_diff) > $k[$i][2]) ) Then $iFound = $i
	WEnd

	If( $iFound = 16 ) Then $iFound = 13

	Return( $iFound )
EndFunc

Func collect_online()
	Debug( "[RES] --- Online einsammeln ---" )

	close_all_windows()

	If( search_pattern( "common/icon_online" ) ) Then
		mouse_move_relative( 0, 0, True )
		wait_sec( 1 )
		If( search_pattern( "common/online_title" ) ) Then
			mouse_move_relative( 210, 110, True )
			mouse_move_relative( 0, 80, True )
			mouse_move_relative( 0, 80, True )
			mouse_move_relative( 0, 80, True )
			mouse_move_relative( 0, 70, True )
		EndIf
	EndIf

	close_all_windows()

	center_fleet()
	
	Debug( "[RES] --- Online einsammeln ENDE ---" )
EndFunc

Func open_signs()
	Debug( "[MAIN] --- Open Signs ---" )

	close_all_windows()

	If( Not search_pattern( "inv/icon_inv" ) ) Then Return
	If( Not wait_for_window( "inv/icon_inv", "inv/title_lager" ) ) Then Return

	$bDo = True
	While( $bDo )
		If( Not search_pattern( "inv/inv_abzeichen" ) ) Then $bDo = False

		If( $bDo ) Then
			MouseMove( $_x, $_y, 0 )
			MouseClick( "left" )
			MouseClick( "left" )
			wait_sec( 1 )
			If( search_pattern( "inv/title_benutzen" ) ) Then
				mouse_move_relative( 20, 220, True )
				$bDo = True
			EndIf
			If( search_pattern( "inv/title_kiste" ) ) Then
				mouse_move_relative( -70, 310, True )
				$bDo = True
			EndIf
		EndIf
	WEnd

	$bDo = True
	While( $bDo )
		If( Not search_pattern( "inv/inv_kiste_abzeichen" ) ) Then $bDo = False

		If( $bDo ) Then
			MouseMove( $_x, $_y, 0 )
			MouseClick( "left" )
			MouseClick( "left" )
			wait_sec( 1 )
			If( search_pattern( "inv/title_kiste" ) ) Then
				mouse_move_relative( -70, 310, True )
				$bDo = True
			EndIf
		EndIf
	WEnd

	close_all_windows()

	Debug( "[MAIN] --- Open Signs ENDE ---" )
EndFunc

Func do_verstaerken()
   ; Stufe 5 auf 6: ca. 7200
	While 1
		If( search_pattern( "common/10x_verstaerken" ) ) Then	mouse_move_relative( 0, 0, True )
		If( search_pattern( "common/button_ok" ) ) Then	mouse_move_relative( 0, 0, True )
		wait_sec( 0.3 )
	Wend
EndFunc

Func send_shuttles()
	Debug( "[MAIN] --- Send Shuttles ---" )

	load_config_file()

	If( search_pattern( "common/bruecke" ) ) Then
		mouse_move_relative( 0, 00, true )
		wait_sec( 1 )

		If( search_pattern( "bruecke/fracht" ) ) Then
			mouse_move_relative( 0, 20, True )
			wait_sec( 1 )

			$bEnd = false
			$i = 0
			While( (Not $bEnd) And ($i < 10) )
				$i = $i + 1
				If( search_pattern( "bruecke/aussenden" ) ) Then
					mouse_move_relative( 0, 0, true )
					wait_sec( 0.5 )
					Debug( "[MAIN] Sende Shuttle" )

					If( $_send_shuttle_next_side = 1 ) Then
						If( search_pattern( "bruecke/rechts" ) ) Then mouse_move_relative( 0, 0, true )
					EndIf

					; Schicken
					Switch $_send_shuttle_postition
						Case 1
							If( search_pattern( "bruecke/aussenden-10-1" ) ) Then mouse_move_relative( 0, 0, true ); Links
						Case 2
							If( search_pattern( "bruecke/aussenden-10-2" ) ) Then mouse_move_relative( 0, 0, true ); Mitte
						Case 3
							If( search_pattern( "bruecke/aussenden-10-3" ) ) Then mouse_move_relative( 0, 0, true ); Rechts
					EndSwitch
				Else
					$bEnd = True
				EndIf
				
				wait_sec( 0.5 )
			WEnd
		EndIf
	EndIf

	close_all_windows()

	If( search_pattern( "res/flotte" ) ) Then mouse_move_relative( 0, 0, True )

	Debug( "[MAIN] --- Send Shuttles ENDE ---" )
EndFunc

Func move_to_base()
	Debug( "[FAILSAFE] Fliege zu Basis" )

	close_all_windows()
	center_fleet()
	wait_sec( 1 )

	; Flug abbrechen
	$i = 0
	$xoffset = 0
	While search_pattern( "common/flotte_fliegt" ) And ($i < 10)
		$i = $i + 1
		Debug( "[MAIN] Flug abbrechen: " & $i )

		If( search_pattern( "common/flotte_fliegt" ) ) Then
			If( $_found_pattern = 3 ) Then $xoffset = 230

			mouse_move_relative( $xoffset, 0, True )
			wait_sec( 1 )
			mouse_move_relative( 600, 180, True )
			wait_sec( 1 )
		EndIf
	Wend
	close_all_windows()
	center_fleet()
	wait_sec( 1 )

	; Beutemission abbrechen
	$i = 0
	While search_pattern( "beute/icon_beute_info" ) And ($i < 10)
		$i = $i + 1
		Debug( "[MAIN] Beutemission abbrechen: " & $i )

		If( search_pattern( "beute/icon_beute_info" ) ) Then
			mouse_move_relative( 0, 0, True )
			wait_sec( 1 )

			If( search_pattern( "beute/verwerfen" ) ) Then
				mouse_move_relative( 0, 0, True )
				wait_sec( 1 )

				If( search_pattern( "beute/verwerfen_message" ) ) Then
				  mouse_move_relative( -70, 140, True )
				  wait_sec( 1 )
				EndIf
			EndIf	
		EndIf
	Wend
	close_all_windows()
	center_fleet()
	wait_sec( 1 )

	If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative( -180, 40, True )
	wait_sec( 1 )

	If( search_pattern( "frage/sek0" ) ) Then
		Debug( "[MAIN] Fliege Sektor 0" )

		mouse_move_relative( 0, 50, True, True )
		wait_sec( 1 )
		mouse_move_relative( -120, 0, True )
		wait_sec( 1 )

		If( search_pattern( "farm/sektor_verlassen" ) ) Then ; Sektor verlassen Abfrage
			Debug( "[MAIN] Sektor verlassen" )

			mouse_move_relative( -70, 140, True )
			wait_sec( 5 )
		EndIf
	EndIf
EndFunc

Func move_to_sektor1()
	Debug( "[FAILSAFE] Fliege zu Sektor 1" )
	reset_mouse()

	If( search_pattern( "frage/worldmap" ) ) Then mouse_move_relative( 0, 0, True )
	wait_sec( 1 )

	If( search_pattern( "beute/sektor-1" ) ) Then
		mouse_move_relative( 0, 50, True, True )
		wait_sec( 1 )
		mouse_move_relative( -120, 20, True )
		wait_sec( 1 )

		If( search_pattern( "farm/sektor_verlassen" ) ) Then ; Sektor verlassen Abfrage
			mouse_move_relative( -70, 140, True )
			wait_sec( 5 )
		EndIf
	EndIf

	; Zentrieren
	center_fleet()
EndFunc

Func switch_to_offi( $strWhich )
	Debug( "[MAIN] --- Switch Offi ---" )

	close_all_windows()

	If( wait_for_window( "common/icon_flotte", "common/icon_flag_settings" ) ) Then
		If( wait_for_window( "common/icon_flag_settings", "common/schiffsdetails" ) ) Then
			If( Not search_pattern( "offi/" & $strWhich & "_big" ) ) Then
				search_pattern( "common/schiffsdetails" )
				mouse_move_relative( -180, 100, True )
				wait_sec( 0.5 )
				If( search_pattern( "offi/befehligt" ) ) Then mouse_move_relative( 0, 0, True )

				$i = 0
				$bFound = False
				Debug( "Suche Offi --> " & $strWhich )
				While( (Not $bFound) And ($i < 7) )
					$i = $i + 1

					If( search_pattern( "offi/" & $strWhich ) ) Then
						mouse_move_relative( 0, 0, True )
						$bFound = True
						Debug( "Offi --> " & $strWhich )
					EndIf
					If( Not $bFound ) Then
						If( search_pattern( "offi/_down" ) ) Then
							MouseMove( $_x, $_y, 0 )
							For $j = 1 To 15
								MouseClick( "left" )
								Sleep( 10 )
							Next
						EndIf
					EndIf
					wait_sec( 2 )
				WEnd
			EndIf
		EndIf
	EndIf

	close_all_windows()

	Debug( "[MAIN] --- Switch Offi ENDE ---" )
EndFunc

Func event_starting_soon()
	$hour = Int( @HOUR )
	$min = Int( @MIN )
	$bExit =	False

	If( $_standalone ) Then Return

	; Boss
	If( (($hour = 5) Or ($hour = 11) Or ($hour = 18)) And ($min >= 50) ) Then $bExit = True ; Pre-Boss
	If( (($hour = 6) Or ($hour = 12) Or ($hour = 19)) And ($min <	50) ) Then $bExit = True ; Boss Running

	; Sys
	If( (($hour = 6) Or ($hour = 12) Or ($hour = 19)) And ($min >= 50) ) Then $bExit = True ; Pre-Sys
	If( (($hour = 7) Or ($hour = 13) Or ($hour = 20)) And ($min <	40) ) Then $bExit = True ; Sys Running

	; 3v3
	If( @WDAY <> 2 ) Then
		If( (($hour = 8) Or ($hour = 14) Or ($hour = 21)) And ($min >= 37) ) Then $bExit = True ; Pre-3v3
	EndIf

	; Neuer Tag
	If( ($hour = 1) And ($min >= 50) ) Then $bExit = True

	If( $bExit ) Then Debug( "[MAIN] Cancel -> Event Starting Soon/Running" )

	Return( $bExit )
EndFunc

Func hotkey_do_screenshot()
	do_screenshot( "manual" )
EndFunc

Func do_screenshot( $strPrefix = "" )
	If( $strPrefix <> "" ) Then $strPrefix = $strPrefix & "_"

	$strFilename = "logs_pics/" & $strPrefix & @YEAR & "-" & @MON & "-" & @MDAY & "_" & @HOUR & "_" & @MIN & "_" & @SEC & ".jpg"
	_ScreenCapture_Capture( $strFilename )

	Debug( "[MAIN] Screenshot (" & $strFilename & ")" )
EndFunc

Func start_browser()
	If( search_pattern( "common/fullscreen_abbrechen", 0, 100 ) ) Then
		Debug( "[MAIN] Klick Fullscreen abbrechen" )
		mouse_move_relative( 80, 50, True )
	EndIf

	If( (WinActivate ( "Star Trek"	) = 0) And (WinActivate ( "startrek"	) = 0) ) Then
		; Kein Window gefunden
		Debug( "Start Browser" )
		ShellExecute( "http://startrek.gamesamba.com/de/default/game?server=s33" )
		wait_for_game_ready()
	EndIf
	
	If( search_pattern( "common/login_click_here", 0, 130 ) ) Then
		mouse_move_relative( 0, 0, True )
		Debug( "[DC] Flash Reload" )
		wait_sec( 1 )

		If( search_pattern( "common/icon_flash", 0, 130 ) ) Then
			Debug( "[DC] Icon Flash Reload" )
			mouse_move_relative( 120, 40, True )
			
			wait_for_game_ready()			
		Endif
	Endif
	
	If( search_pattern( "common/reload" ) ) Then
		Debug( "[MAIN] Klick Neu Laden" )
		mouse_move_relative( 0, 0, True )

		wait_for_game_ready()
	EndIf	
EndFunc

Func get_new_mission()
	$bAktiv = False
	$i = 0
	While (Not $bAktiv) And ($i < 10)
		$i = $i + 1 
		If( search_pattern( "beute/icon_beute_info" ) ) Then $bAktiv = True
		wait_sec( 0.5 )
	Wend
	If( $bAktiv ) Then
		Debug( "[BM] Beutemission Aktiv" )
		Return( -1 )
	EndIf	
	Debug( "[BM] KEINE Beutemission Aktiv" )

	$hour = Int( @HOUR )
	$min = Int( @MIN )
	If( (($hour = 1) Or ($hour = 5) Or ($hour = 11) Or ($hour = 18)) And ($min >= 30) ) Then
		Debug( "[BM] Beuteabbruch -> Event" )
		Return( -3 )
	EndIf

	If( Not search_pattern( "beute/mission_bruecke" ) ) Then
		Debug( "[BM] Keine Beutemission da" )
		Return( -2 )
	EndIf
	
	mouse_move_relative( 0, 0, True )
	wait_sec( 1 )

	If( search_pattern( "beute/get_beute_mission" ) ) Then
		Debug( "[BM] Hole neue Beutemission" )

		mouse_move_relative( 0, 60, True )
		wait_sec( 0.5 )
		If( search_pattern( "beute/abholen" ) ) Then mouse_move_relative( 0, 0, True )
		wait_sec( 0.5 )
		If( search_pattern( "beute/starten" ) ) Then mouse_move_relative( 0, 0, True )
		wait_sec( 0.5 )

		close_all_windows()

		; Zentrieren
		If( search_pattern( "sys/sys_flotte", 0 ) ) Then mouse_move_relative( 0, 0, True )

		Debug( "[BM] Beutemission abgeholt" )

		wait_sec( 1 )
		$min = Int( @MIN )
		$hour = Int( @HOUR )
		If( (($min >= 5) And ($min <= 25)) Or (($min >= 35) And ($min <= 55)) ) Then
			$temp_hour = (2 * $hour + Floor( $min / 30 )) / 2
			send_shuttles()
			write_debug_window_data(	9, $temp_hour )
		EndIf
		Return( 1 )
	EndIf

	Return( -4 )
EndFunc

Func center_fleet()
	; Zentrieren
	If( search_pattern( "sys/sys_flotte" ) ) Then mouse_move_relative( 0, 0, True )
EndFunc

Func pause( $iMaxMinPause = 10 )
	If( Not IsDeclared( "iMaxMinPause" ) ) Then $iMaxMinPause = 10

	$iStarttime = get_timestamp()
	$_bPause = Not $_bPause
	If( $_bPause ) Then
		Debug( "[MAIN] Pause ON (max " & $iMaxMinPause & " min)" )
	Else
		Debug( "[MAIN] Pause OFF" )
	EndIf

	While $_bPause And ((get_timestamp() - $iStarttime) < ($iMaxMinPause * 60))
		wait_sec( 0.5 )
	WEnd

	If( $_bPause ) Then
		Debug( "[MAIN] Pause Auto OFF" )
		$_bPause = False
	EndIf
EndFunc

Func collect_css_res()
	Debug( "[RES] --- CSSS einsammeln ---" )

	close_all_windows()

	If( Not wait_for_window( "sys/icon_css", "sys/button_belohnung" ) ) Then Return
	If( Not wait_for_window( "sys/button_belohnung", "sys/button_teilnahmen" ) ) Then Return

	For $k = 1 To 2
		For $i = 1 To 2
		While( search_pattern( "sys/button_abholen" ) )
			mouse_move_relative( 0, 0, True, True )
			wait_sec( 1 )
		WEnd

		If( ($i = 1) And search_pattern( "sys/button_down", 0, 40 ) ) Then
			wait_sec( 1 )
			MouseMove( $_x, $_y, 0 )
			For $j = 1 To 7
				MouseClick( "left" )
				Sleep( 10 )
			Next
			MouseMove( $_x, $_y+20, 0 )
		EndIf
		Next

		If( ($k = 1) And search_pattern( "sys/button_teilnahmen" ) ) Then mouse_move_relative( 0, 0, True )
	Next

	close_all_windows()

	center_fleet()
	
	Debug( "[RES] --- CSSS einsammeln ENDE ---" )
EndFunc

Func change_weapon_set( $dest )
	Debug( "[MAIN] --- Change to Set " & $dest & " ---" )

	close_all_windows()

	If( Not wait_for_window( "common/icon_faehig", "common/set-1" ) ) Then Return

	Switch $dest
		Case 1
			If( search_pattern( "common/set-1" ) ) Then mouse_move_relative( 0, 0, True )
		Case 2
			If( search_pattern( "common/set-2" ) ) Then mouse_move_relative( 0, 0, True )
	EndSwitch

	wait_sec( 1 )

	; Verschiebe Fenster
	AutoItSetOption( "MouseClickDragDelay", 500 )
	MouseClickDrag( $MOUSE_CLICK_LEFT, $_x-230, $_y-500, $_x-230, $_y-470, 1 )
	wait_sec( 1 )

	If( search_pattern( "common/set_active" ) ) Then mouse_move_relative( 0, 0, True )

	close_all_windows()

	MouseMove( Round( (@DesktopWidth - 1920) / 2 ) + 10, Round( (@DesktopHeight - 1080 ) / 2 ) + 110, 2 )
	MouseClick( "left" );

	center_fleet()

	Debug( "[MAIN] --- Change to Set " & $dest & " ENDE ---" )
EndFunc

Func collect_daily()
	Debug( "[DAY] --- Tagesaufgaben ---" )

	close_all_windows()

	If( wait_for_window( "common/icon_daily", "common/title_daily" ) ) Then
		mouse_move_relative( 220, 350, True )
		mouse_move_relative( 0, 0, True )
		mouse_move_relative( 0, 0, True )
		wait_sec( 1 )
	EndIf

	close_all_windows()
	
	Debug( "[DAY] --- Tagesaufgaben ENDE ---" )
EndFunc

Func collect_events()
	Debug( "--- Events ---" )

	close_all_windows()

	If( search_pattern( "events/icon_gluecksfeld" ) ) Then
		Debug( "[EV] Gluecksfeld" )
		mouse_move_relative( 0, 0, True )
		wait_sec( 1 )
	EndIf

	close_all_windows()

	If( search_pattern( "events/icon_sturm_sektor" ) ) Then
		Debug( "[EV] Sturm Sektor" )
		mouse_move_relative( 0, 0, True )
		wait_sec( 3 )
		If( search_pattern( "events/sturm_sektor_frei" ) ) Then 
			Debug( "[EV] Freispiel" )
			mouse_move_relative( 0, 0, True )
			wait_sec( 3 )
		EndIf
	EndIf

	close_all_windows()

	If( search_pattern( "events/icon_gluecksrad" ) ) Then
		Debug( "[EV] Gluecksrad" )
		mouse_move_relative( 0, 0, True )
		wait_sec( 3 )
		If( search_pattern( "events/gluecksrad_frei" ) ) Then 
			Debug( "[EV] Freispiel" )
			mouse_move_relative( 0, 0, True )
			wait_sec( 3 )
		EndIf
	EndIf

	close_all_windows()
	
	Debug( "--- Events ENDE ---" )
EndFunc

Func search_offi_stats( $type = "ausweichen" )
	Debug( "[OFFI] search..." )

	$bStop = False
	$bFound = False
	While( Not $bStop )
		If( ($type = "ausweichen") And search_pattern( "offi/ausweichen_10pr" ) ) Then $bFound = True
		If( $type = "regeneration" ) Then
			If( search_pattern( "offi/regeneration_6pr" ) ) Then $bStop = True
			If( search_pattern( "offi/regeneration" ) ) Then $bFound = True
		EndIf

		If( $bFound ) Then
			Debug( "[OFFI] found " & $type )
			pause()
			Debug( "[OFFI] search..." )
			$bFound = False
		EndIf

		If( Not $bStop ) Then
			If( search_pattern( "offi/reset" ) ) Then
				mouse_move_relative( 0, 0, True )
				wait_sec( 0.5 )
			EndIf
		EndIf
	Wend
EndFunc

Func do_arena_att()
	Debug( "[ARENA] --- Start Arena --- " )

	close_all_windows()

	If( wait_for_window( "arena/icon_arena", "arena/title_arena" ) ) Then
		If( search_pattern( "arena/atts_0" ) ) Then
			Debug( "[ARENA] keine Angriffe mehr" )
		Else
			If( search_pattern( "arena/angriff" ) ) Then ; linke Seite Angriff
				Debug( "[ARENA] Arena Angriff" )
				mouse_move_relative( 0, 0, True )

				$iStarttime = get_timestamp()
				While (Not search_pattern( "kampf/verlassen" )) And ((get_timestamp() - $iStarttime) < 120)
					Debug( "[ARENA] warte auf Kampfende" )
					wait_sec( 1 )
				Wend

				If( search_pattern( "kampf/verlassen" ) ) Then mouse_move_relative( 0, 0, True )
			Else
				Debug( "[ARENA] Arena Angriff nicht moeglich" )
			EndIf
		EndIf
	EndIf

	wait_sec( 1 )
	close_all_windows()

	center_fleet()
	
	Debug( "[ARENA] --- Start Arena ENDE --- " )
EndFunc

Func arena_store()
	Debug( "[ARENA] --- Arena Speichern --- " )

	close_all_windows()

	If( wait_for_window( "arena/icon_arena", "arena/title_arena" ) ) Then
		If( search_pattern( "arena/speichern" ) ) Then
			mouse_move_relative( 0, 0, True )
			Debug( "[ARENA] Arena Save" )
		EndIf
	EndIf
	close_all_windows()

	If( wait_for_window( "arena/icon_staffel_csss", "arena/speichern" ) ) Then
		If( search_pattern( "arena/speichern" ) ) Then
			mouse_move_relative( 0, 0, True )
			Debug( "[ARENA] CSSS Save" )
		EndIf
	EndIf
	close_all_windows()

	Debug( "[ARENA] --- Arena Speichern ENDE --- " )
EndFunc

Func wait_for_game_ready()
	global $_reload_wait_timer
	
	Debug( "[RELOAD] --- Wait for Game (max " & $_reload_wait_timer & "s) ---" )

	$iStarttime = get_timestamp()
	$bGameLoaded = False
	While (Not $bGameLoaded) And ((get_timestamp() - $iStarttime) < $_reload_wait_timer)
		close_all_windows()

		; Anmelden
		If( search_pattern( "common/bitte_anmelden", 0, 130 ) ) Then 
			mouse_move_relative( 330, 40, True )
		EndIf
		If( search_pattern( "common/anmelden", 0, 130 ) ) Then 
			mouse_move_relative( 0, 0, True )
			wait_sec( 1 )
			mouse_move_relative( -600, 460, True )
			Send( "+{HOME}" )
			Send( "games@sordje.at" )
			Send("{TAB}")
			Send( "bn-42riq" )
			Send("{TAB}{TAB}{TAB}{SPACE}")
			wait_sec( 5 )
			ShellExecute( "http://startrek.gamesamba.com/de/default/game?server=s33" )
		EndIf

		; Flash Error Login
		If( search_pattern( "common/login_click_here", 0, 130 ) ) Then
			mouse_move_relative( 0, 0, True )
			Debug( "[DC] Flash Reload" )
			wait_sec( 1 )
		Endif

		If( search_pattern( "common/icon_flash", 0, 130 ) ) Then
			Debug( "[DC] Icon Flash Reload" )
			mouse_move_relative( 120, 40, True )
		Endif
		
		If( ((get_timestamp() - $iStarttime) >= 10) And search_pattern( "common/symbol_settings" ) ) Then
			$bGameLoaded = True
			Debug( "[RELOAD] --- Game Loaded ---" )
		EndIf
		If( ((get_timestamp() - $iStarttime) >= 10) And search_pattern( "sys/sys_flotte" ) ) Then
			$bGameLoaded = True
			Debug( "[RELOAD] --- Game Loaded (crashed) ---" )
		EndIf
		
		If( Not $bGameLoaded ) Then
			wait_sec( 1 )
		EndIf
	Wend

	If( Not $bGameLoaded ) Then
		Debug( "[RELOAD] Chrome Hardkill" )
		While( ProcessExists( "chrome.exe" ) )
			$iPid = ProcessExists( "chrome.exe" )
			ProcessClose( $iPid )
		Wend
		wait_sec( 3 )
		start_browser()
	EndIf

	Debug( "[RELOAD] --- Game Loaded: " & (get_timestamp() - $iStarttime) & "s ---" )
EndFunc

Func collect_messages()
	Debug( "[MESS] --- Nachrichten --- " )

	close_all_windows()

	If( search_pattern( "nachrichten/icon_nachrichten", 0, 70 ) ) Then
		mouse_move_relative( 0, 0, True )
		wait_sec( 0.5 )
		If( search_pattern( "nachrichten/system", 0, 70 ) ) Then
			mouse_move_relative( 0, 0, True )
			wait_sec( 0.5 )
			mouse_move_relative( 35, 390, True )
			wait_sec( 0.5 )
			mouse_move_relative( -115, 0, True )
			wait_sec( 0.5 )
		EndIf
	EndIf

	close_all_windows()

	center_fleet()
	
	Debug( "[MESS] --- Nachrichten ENDE --- " )
EndFunc

Func collect_staffel()
	Debug( "[MESS] --- Staffel --- " )

	close_all_windows()

	If( wait_for_window( "staffel/icon_staffel", "staffel/titel_staffel" ) ) Then
		; Melden
		If( wait_for_window( "staffel/button_melden", "staffel/titel_melden" ) ) Then
			Debug( "[STAFFEL] Melden" )
			If( search_pattern( "staffel/titel_melden" ) ) Then
				mouse_move_relative( -253, 370, True )
				wait_sec( 0.5 )
				mouse_move_relative( 537, 14, True )
				wait_sec( 0.5 )
			EndIf
		EndIf
	EndIf
	close_all_windows()

	If( wait_for_window( "staffel/icon_staffel", "staffel/titel_staffel" ) ) Then
		; Spenden
		If( wait_for_window( "staffel/button_spenden", "staffel/titel_spenden" ) ) Then
			Debug( "[STAFFEL] Spenden" )
			If( search_pattern( "staffel/titel_spenden" ) ) Then
				; Metall
				mouse_move_relative( 131, 82, True )
				wait_sec( 0.5 )
				
				; Kristall
				mouse_move_relative( 0, 85, True )
				wait_sec( 0.5 )
				
				; Syn
				$y = 170
				If( $_staffel_spend_syn ) Then
					mouse_move_relative( 0, 85, True )
					wait_sec( 0.5 )
					$y = 85
				EndIf
				
				; Marken
				mouse_move_relative( 0, $y, True )
				wait_sec( 0.5 )
				
				; Spenden
				mouse_move_relative( 48, 174, True )
				wait_sec( 0.5 )
			EndIf
		EndIf
	EndIf
	close_all_windows()

	If( wait_for_window( "staffel/icon_staffel", "staffel/titel_staffel" ) ) Then
		; Kaufen
		If( search_pattern( "staffel/titel_staffel" ) ) Then
			Debug( "[STAFFEL] Kaufen" )

			mouse_move_relative( 47, 49, True )
			wait_sec( 0.5 )
			
			load_config_file()
			
			For $i = 1 To 5
				If( $_staffel_buy_vip And search_pattern( "staffel/shop_vip" ) ) Then mouse_move_relative( 138, 88, True )
				If( $_staffel_buy_kit5 And search_pattern( "staffel/shop_umform" ) ) Then mouse_move_relative( 138, 88, True )
				wait_sec( 0.5 )

				If( search_pattern( "staffel/titel_staffel" ) ) Then mouse_move_relative( 84, 424, True )
				wait_sec( 0.5 )
			Next
		EndIf
	EndIf
	close_all_windows()

	Debug( "[MESS] --- Staffel ENDE --- " )
EndFunc

Func reload_torp()
	Debug( "[ATT] --- Reload Torp --- " )

	For $i = 1 To 3 
		close_all_windows()
		wait_sec( 1 )
	Next

	If( wait_for_window( "common/icon_flotte", "common/icon_flag_settings" ) ) Then
		If( wait_for_window( "common/icon_flag_settings", "common/schiffsdetails" ) ) Then
		  If( wait_for_window( "kampf/reload_torp", "kampf/torp_title" ) ) Then
				If( search_pattern( "kampf/reload_torp" ) ) Then
					; Nachladen
					Debug( "[ATT] Reload" )
					
					mouse_move_relative( 0, 280, True )
					wait_sec( 0.5 )
				EndIf
			EndIf
		EndIf
	EndIf

	close_all_windows()

	center_fleet()
	
	Debug( "[ATT] --- Reload Torp ENDE --- " )
EndFunc
