#include-once

Func get_array_for_pattern( $img_path, ByRef $arr )
	$arr = 0

	Switch $img_path
		Case "img/common/bitte_anmelden.bmp"
			$arr = get_array( 718, -20, 1203, 128 )
		Case "img/common/anmelden.bmp"
			$arr = get_array( 1564, -8, 1748, 88 )
;		Case "img/common/anmelden.bmp"
;			$arr = get_array( 753, 386, 1164, 698 )
		Case "img/common/login_click_here.bmp"
			$arr = get_array( 656, 0, 1350, 860 )
		Case "img/common/icon_flash.bmp"
			$arr = get_array( -202, 14, -169, 45 )
		Case "img/common/teamviewer_ok.bmp"
			$arr = get_array( 680, 400, 1300, 700 )

		; Box in Mitte -> Text
		Case "img/farm/sektor_verlassen.bmp", "img/farm/sektor_verlassen_2.bmp", "img/farm/flotte_bewegen.bmp", "img/boss/punkte.bmp", "img/boss/punkte_2.bmp", _
			"img/boss/buy_att.bmp", "img/common/check_dc.bmp", "img/common/check_dc_2.bmp", "img/3v3/3v3_abfrage.bmp", "img/3v3/3v3_wait.bmp", _
			"img/sys/sektor_betreten.bmp", "img/sys/sys_abfrage_gegenstaende.bmp", "img/heraus/her_ruecksetzen.bmp", "img/boss/boss_aufgeben_text.bmp", _
			"img/sys/center_ship.bmp", "img/sys/sys_aktualisieren.bmp", "img/shuttle/shu_restart.bmp", "img/sys/sys_abbrechen.bmp", "img/sys/sys_repair_text.bmp", _
			"img/sys/sys_abbrechen_2.bmp", "img/sys/kampf_teilnehmen.bmp", "img/common/shockwave_crashed.bmp"
			$arr = get_array( 740, 510, 1160, 650 )
		Case "img/offi/reset.bmp", "img/offi/ausweichen_10pr.bmp", "img/offi/regeneration_6pr.bmp", "img/offi/regeneration.bmp"
			$arr = get_array( 840, 498, 1053, 733 )
			
		; Rechts Unten
		Case "img/res/base_details.bmp", "img/res/flotte.bmp", "img/sys/sys_flotte.bmp", "img/sys/sys_flotte_2.bmp", "img/common/bruecke.bmp", "img/common/bruecke_2.bmp"
			$arr = get_array( 1710, 800, 1920, 1080 )
			
		; Oben Mitte
		Case "img/boss/boss_start.bmp", "img/boss/boss_staffel_start.bmp", "img/3v3/3v3_start.bmp", "img/sys/sys_start.bmp"
			$arr = get_array( 760, 180, 1330, 380 )
			
		; Chat Box
		Case "img/sys/sys_start_text.bmp", "img/boss/boss_korvac_dead.bmp"
			$arr = get_array( 0, 780, 390, 1010 )
			
		; Rechts Oben - Map
		Case "img/farm/suche.bmp", "img/common/mini_map_location.bmp", "img/common/symbol_settings.bmp"
			$arr = get_array( 1700, 100, 1920, 390 )
			
		; Icon Leiste
		Case "img/heraus/icon_heraus.bmp", "img/common/icon_online.bmp", "img/inter/icon_interstellar.bmp", "img/sys/icon_css.bmp", "img/common/icon_daily.bmp", _
			"img/events/icon_gluecksfeld.bmp", "img/events/icon_sturm_sektor.bmp", "img/trials/icon_trials.bmp", "img/arena/icon_arena.bmp", _
			"img/arena/icon_staffel_csss.bmp", "img/nachrichten/icon_nachrichten.bmp", "img/events/icon_gluecksrad.bmp"
			$arr = get_array( 264, 115, 1718, 231 )
			
		; Icon Leiste Unten
		Case "img/common/icon_flotte.bmp", "img/shuttle/icon_shuttle.bmp", "img/shuttle/icon_shuttle_2.bmp", "img/inv/icon_inv.bmp", "img/common/icon_faehig.bmp", _ 
			"img/staffel/icon_staffel.bmp"
			$arr = get_array( 1000, 960, 1730, 1080 )
			
		; fast ganz oben
		Case "img/common/fight_continue.bmp"
			$arr = get_array( 440, 190, 750, 290)
			
		; Unterer Bereich
		Case "img/common/icon_info_message.bmp", "img/common/icon_email_message.bmp"
			$arr = get_array( 600, 880, 1210, 1000 )
			
		; Mittlerer Bereich
		Case "img/common/button_close.bmp"
			$arr = get_array( 380, 160, 1920, 1080 )
		Case "img/common/chrome_crash.bmp"
			$arr = get_array( 380, 250, 1540, 1080 )
		Case "img/common/online_abholen.bmp", "img/common/online_title.bmp", "img/inv/title_lager.bmp", "img/inv/inv_abzeichen.bmp", _
			"img/inv/title_benutzen.bmp", "img/inv/title_kiste.bmp", "img/inv/inv_kiste_abzeichen.bmp", "img/common/title_daily.bmp"
			$arr = get_array( 380, 250, 1540, 900 )
		Case "img/farm/title_beutemission.bmp"
			$arr = get_array( 380, 250, 1440, 900 )

		; Skillleiste
		Case "img/kampf/att_start.bmp", "img/kampf/att_ueberladung.bmp", "img/kampf/att_emp.bmp", "img/kampf/att_aps.bmp", "img/kampf/att_titan.bmp", _
			"img/kampf/att_emp_inaktiv.bmp", "img/kampf/att_sense.bmp", "img/kampf/att_pn.bmp", "img/kampf/att_pandora.bmp"
			$arr = get_array( 795, 920, 1090, 975 )

		; Kampf Verlassen
		Case "img/kampf/verlassen.bmp", "img/kampf/verlassen_2.bmp", "img/kampf/schliessen.bmp"
			$arr = get_array( 800, 730, 1120, 790 )

		; DC
		Case "img/common/false_conn.bmp"
			$arr = get_array( 0, 420, 240, 650 )
		Case "img/common/reload.bmp", "img/common/wartung.bmp", "img/common/fullscreen_abbrechen.bmp"
			$arr = get_array( 700,	45, 1200, 150 )
		Case "img/common/server-33.bmp", "img/common/server-33-wartung.bmp"
			$arr = get_array( 740, 330, 1410, 451 )
		Case "img/common/no_inet.bmp"
			$arr = get_array( 630, 180, 1020, 300 )

		; Ressourcen
		Case "img/res/res_syn.bmp", "img/res/res_syn_3.bmp", "img/res/res_deut.bmp", "img/res/res_deut_3.bmp", "img/res/res_met.bmp", "img/res/res_met_2.bmp", _
			"img/res/res_met_3.bmp", "img/res/res_kryst.bmp", "img/res/res_kryst_2.bmp", "img/res/res_kryst_3.bmp"
			$arr = get_array( 360, 380, 1480, 1080 )

		; Flotte
		Case "img/common/icon_flag_settings.bmp"
			$arr = get_array( 1010, 580, 1080, 640 )
		Case "img/common/schiffsdetails.bmp", "img/common/ausruestung.bmp", "img/common/flotte_rep.bmp", "img/common/reparieren.bmp", _
			"img/common/rep_kit_1.bmp", "img/common/rep_kit_1_sys.bmp", "img/kampf/reload_torp.bmp", "img/kampf/torp_title.bmp"
			$arr = get_array( 380, 330, 1540, 830 )
		Case "img/common/rep_finished.bmp"
			$arr = get_array( 580, 380, 700, 550 )
		Case "img/common/phaserkanone_76.bmp", "img/common/turm_70.bmp", "img/common/disruptorkanone_78.bmp", "img/common/turm_80.bmp", "img/common/turm_90.bmp", "img/common/emp_70.bmp", _
			"img/common/antiprotonenkanone_80.bmp", "img/common/pandora_85.bmp", "img/common/emp_antiproton.bmp"
			$arr = get_array( 1180, 390, 1280, 810 )

		; Boss
		Case "img/boss/boss_angriff_shuttle.bmp"
			$arr = get_array( 730, 630, 960, 770 )
		Case "img/boss/last_0.bmp", "img/boss/last_0_2.bmp", "img/boss/last_1.bmp", "img/boss/last_2.bmp", "img/boss/last_3.bmp", "img/boss/last_4.bmp", _
			"img/boss/last_5.bmp", "img/boss/last_6.bmp", "img/boss/last_7.bmp", "img/boss/last_7_2.bmp", "img/boss/last_8.bmp", "img/boss/last_9.bmp", _
			"img/boss/last_10.bmp", "img/boss/last_11.bmp", "img/boss/last_12.bmp", "img/boss/last_12_2.bmp", "img/boss/button_angreifen.bmp"
			$arr = get_array( 850, 880, 1120, 950 )
		Case "img/boss/boss_angriff_moeglich.bmp", "img/boss/boss_angriff_moeglich_2.bmp"
			$arr = get_array( 1150, 880, 1290, 950 )
		Case "img/boss/boss_close.bmp"
			$arr = get_array( 1460, 160, 1560, 220 )
		Case "img/boss/boss_aufgeben.bmp"
			$arr = get_array( 1060, 880, 1250, 940 )
		Case "img/boss/in_boss_screen.bmp"
			$arr = get_array( 200, 230, 310, 290 )
		Case "img/boss/start_boss_angreifen.bmp"
			$arr = get_array( 1030, 730, 1200, 790 )
		Case "img/boss/start_boss_staffel_angreifen.bmp"
			$arr = get_array( 780, 650, 940, 710 )
		Case "img/boss/place_1.bmp", "img/boss/place_2.bmp", "img/boss/place_3.bmp"
			$arr = get_array( 130, 340, 380, 440 )
		Case "img/boss/shield_down.bmp", "img/boss/shield_up.bmp"
			$arr = get_array( 1275, 375, 1395, 395 )

		; 3v3
		Case "img/3v3/3v3_ende.bmp"
			$arr = get_array( 680, 370, 780, 410 )
		Case "img/3v3/3v3_close.bmp"
			$arr = get_array( 1380, 290, 1460, 330 )
		Case "img/3v3/3v3_einzelspiel.bmp", "img/3v3/3v3_einzelspiel_2.bmp"
			$arr = get_array( 910, 710, 1030, 750	)

		; Herausforderung
		Case "img/heraus/angriff.bmp"
			$arr = get_array( 940, 700, 1180, 760 )
		Case "img/heraus/weiter.bmp"
			$arr = get_array( 810, 470, 1080, 572 )
		Case "img/heraus/her_failed.bmp"
			$arr = get_array( 750, 300, 1140, 370 )
		Case "img/heraus/her_finished.bmp"
			$arr = get_array( 510, 300, 1250, 600 )
		Case "img/heraus/her_reset_0_3.bmp", "img/heraus/her_reset_1_3.bmp", "img/heraus/her_reset_2_3.bmp", "img/heraus/her_reset.bmp"
			$arr = get_array( 510, 750, 820, 820 )
		Case "img/heraus/her_normal.bmp", "img/heraus/her_schwer.bmp", "img/heraus/her_experte.bmp"
			$arr = get_array( 520, 350, 1400, 450 )
		Case "img/heraus/heraus_close.bmp"
			$arr = get_array( 1340, 260, 1430, 310 )

		; Sys
		Case "img/sys/sys_autokampf.bmp"
			$arr = get_array( 335, 230, 363, 249 )
		Case "img/sys/sys_start_into.bmp"
			$arr = get_array( 0, 200, 400, 300 )
		Case "img/sys/sys_ap_8.bmp", "img/sys/sys_ap_9.bmp", _
			"img/sys/sys_ap_11.bmp", "img/sys/sys_ap_18.bmp", "img/sys/sys_ap_17.bmp", "img/sys/sys_ap_10.bmp", _
			"img/sys/sys_ap_22.bmp", "img/sys/sys_ap_21.bmp", "img/sys/sys_ap_25.bmp", "img/sys/sys_ap_20.bmp", "img/sys/sys_ap_19.bmp", _
			"img/sys/sys_ap_13.bmp", "img/sys/sys_ap_24.bmp", "img/sys/sys_ap_23.bmp", "img/sys/sys_ap_12.bmp", _
			"img/sys/sys_ap_16.bmp", "img/sys/sys_ap_14.bmp", "img/sys/sys_ap_15.bmp", "img/sys/sys_ap_14_2.bmp"
			$arr = get_array( 0, 200, 1720, 1080 )
		Case "img/sys/sys_ap_7.bmp"
			$arr = get_array( 0, 300, 800, 1080 )
		Case "img/sys/sys_repair.bmp"
			$arr = get_array( 290, 510, 450, 570 )
		Case "img/sys/sys_in_repair.bmp", "img/sys/sys_verfuegbar.bmp", "img/sys/sys_im_kampf.bmp"
			$arr = get_array( 0, 200, 180, 250 )
		Case "img/sys/button_belohnung.bmp", "img/sys/button_teilnahmen.bmp", "img/sys/button_abholen.bmp", "img/sys/button_down.bmp"
			$arr = get_array( 560, 310, 1350, 860 )

		; Shuttle
		Case "img/shuttle/button_close.bmp", "img/shuttle/wingman_ausstattung_text.bmp", "img/shuttle/wingman_alpha.bmp", _
 				"img/shuttle/next.bmp", "img/shuttle/wingman_beta.bmp", "img/shuttle/shuttle_alpha.bmp", "img/shuttle/shuttle_beta.bmp", _
			"img/shuttle/reset_free.bmp", "img/shuttle/repair.bmp", "img/shuttle/shuttle_gamma.bmp", "img/shuttle/shuttle_nu.bmp", _
			"img/shuttle/shuttle_zeta.bmp"
			$arr = get_array( 360, 290, 1550, 870 )
		Case "img/shuttle/shuttle_alpha_big.bmp", "img/shuttle/shuttle_beta_big.bmp", "img/shuttle/shuttle_gamma_big.bmp", _
			"img/shuttle/shuttle_zeta_big.bmp", "img/shuttle/shuttle_nu_big.bmp", "img/shuttle/shuttle_theta_big.bmp", _
			"img/shuttle/shuttle_kappa_big.bmp", "img/shuttle/shuttle_delta_big.bmp"
			$arr = get_array( 490, 500, 960, 680 )
		Case "img/shuttle/system_01_10.bmp", "img/shuttle/system_11_20.bmp", "img/shuttle/system_21_30.bmp"
			$arr = get_array( 820, 350, 1080, 380)
		Case "img/shuttle/ausstattung.bmp", "img/shuttle/shuttle_angriff.bmp"
			$arr = get_array( 870, 730, 1190, 810 )
		Case "img/shuttle/small_shuttle_icon.bmp", "img/shuttle/ship_razor.bmp", "img/shuttle/ship_fregatte.bmp", "img/shuttle/ship_destroyer.bmp", _
			"img/shuttle/ship_frost.bmp", "img/shuttle/ship_expedition.bmp", "img/shuttle/ship_stellarblade.bmp", "img/shuttle/ship_star_chaser.bmp"
			$arr = get_array( 1190, 290, 1550, 870 )

		; Interstellar
		Case "img/inter/title_interstellar.bmp"
			$arr = get_array( 750, 250, 1160, 340 )
		Case "img/inter/back.bmp", "img/inter/stufe_15.bmp", "img/inter/stufe_16.bmp", "img/inter/stufe_19.bmp", "img/inter/finish.bmp"
			$arr = get_array( 720, 320, 1340, 380 )
		Case "img/inter/angriff.bmp"
			$arr = get_array( 1160, 770, 1340, 850 )

		; Verstärken
		Case "img/common/10x_verstaerken.bmp", "img/common/button_ok.bmp"
			$arr = get_array( 1000, 730, 1270, 780 )

		; Bruecke
		Case "img/bruecke/fracht.bmp", "img/bruecke/aussenden.bmp", "img/bruecke/rechts.bmp"
			$arr = get_array( 510, 300, 1520, 840 )
		Case "img/bruecke/aussenden-10-1.bmp"
			$arr = get_array( 577, 710, 821, 770 )
		Case "img/bruecke/aussenden-10-2.bmp"
			$arr = get_array( 830, 710, 1074, 770 )
		Case "img/bruecke/aussenden-10-3.bmp"
			$arr = get_array( 1084, 710, 1327, 770 )

		; Fragezeichen
		Case "img/common/flotte_fliegt.bmp"
			$arr = get_array( 0, 450, 300, 530 )
		Case "img/frage/angriff.bmp"
			$arr = get_array( 1110, 740, 1270, 800 )
		Case "img/frage/worldmap.bmp"
			$arr = get_array( 1650, 100, 1750, 200 )
		Case "img/frage/sek16.bmp", "img/frage/sek18.bmp", "img/frage/sek20.bmp", "img/frage/sek19.bmp", "img/frage/sek17.bmp", "img/frage/sek21.bmp", _
			"img/frage/sek0.bmp", "img/frage/sek15.bmp", "img/frage/sek22.bmp", "img/frage/sek23.bmp", "img/frage/sek24.bmp"
			$arr = get_array( 200, 200, 1700, 900 )
		Case "img/frage/saeubern.bmp", "img/frage/saeubern_2.bmp"
			$arr = get_array( 400, 600, 1200, 900 )

		; Offi
		Case "img/offi/_down.bmp"
			$arr = get_array( 1490, 770, 1530, 810 )
		Case "img/offi/befehligt.bmp", "img/offi/clay_lucas.bmp", "img/offi/jakob_karel.bmp", "img/offi/penelope_patton.bmp", "img/offi/eliza_tony.bmp"
			$arr = get_array( 1180, 330, 1540, 840 )
		Case "img/offi/clay_lucas_big.bmp", "img/offi/jakob_karel_big.bmp", "img/offi/penelope_patton_big.bmp", "img/offi/eliza_tony_big.bmp"
			$arr = get_array( 470, 370, 800, 540 )
		; Beutemission
		Case "img/beute/abschliessen_rechts.bmp"
			$arr = get_array( 1680, 640, 1850, 710 )
		Case "img/beute/sek_angriff.bmp", "img/beute/drohne.bmp", "img/beute/detektor.bmp", "img/beute/detektor_2.bmp", "img/beute/betreten.bmp", "img/beute/anfliegen_all.bmp", _
			"img/beute/wurmloch.bmp", "img/beute/sektor-1.bmp"
			$arr = get_array( 0, 0, 1920, 1080 )
		Case "img/beute/icon_beute_info.bmp"
			$arr = get_array( 220, 310, 310, 390 )
		Case "img/beute/sektorkarte.bmp", "img/beute/pfeil.bmp", "img/beute/sek_anfliegen.bmp"
			$arr = get_array( 370, 300, 1440, 860 )
		Case "img/beute/mission_bruecke.bmp"
			$arr = get_array( 1710, 800, 1920, 1080 )
		Case "img/beute/get_beute_mission.bmp"
			$arr = get_array( 1040, 270, 1240, 450 )
		Case "img/beute/abschliessen.bmp", "img/beute/begib_dich_zu_planetoid.bmp", "img/beute/nahe_der_position.bmp", _
			"img/beute/reise_zu_system.bmp", "img/beute/schliesse_ein_gefecht_bei.bmp", "img/beute/innerhalb_des_systems.bmp", _
			"img/beute/abholen.bmp", "img/beute/starten.bmp", "img/beute/anfliegen_mitte.bmp", "img/beute/verwerfen.bmp", "img/beute/verwerfen_message.bmp", _
			"img/beute/drohne_auf_planetoid.bmp"
			$arr = get_array( 560, 330, 1340, 840 )

		; Fähigkeiten
		Case "img/common/set-1.bmp", "img/common/set-2.bmp", "img/common/set_active.bmp"
			$arr = get_array( 1120, 350, 1340, 840 )
		
		; Trials
		Case "img/trials/angriff.bmp"
			$arr = get_array( 580, 730, 760, 810 )		
		Case "img/trials/finish.bmp", "img/trials/abholen.bmp"
			$arr = get_array( 490, 290, 1410, 870 )		
		
		; Arena
		Case "img/arena/angriff.bmp" 
			$arr = get_array( 814, 710, 986, 764 )
		Case "img/arena/title_arena.bmp" 
			$arr = get_array( 887, 270, 1024, 329 )
		Case "img/arena/atts_0.bmp" 
			$arr = get_array( 1225, 783, 1386, 820 )
		Case "img/arena/speichern.bmp" 
			$arr = get_array( 475, 279, 1430, 879 )
			
		; Nachrichten
		Case "img/nachrichten/system.bmp" 
			$arr = get_array( 575, 322, 1335, 838 )
			
		; Staffel
		Case "img/staffel/button_melden.bmp", "img/staffel/button_spenden.bmp", "img/staffel/shop_vip.bmp", "img/staffel/shop_umform.bmp", _ 
			"img/staffel/titel_melden.bmp", "img/staffel/titel_spenden.bmp", "img/staffel/titel_staffel.bmp"
			$arr = get_array( 556, 290, 1336, 805 )
			
		; Chrome
		Case "img/common/chrome_crash_close.bmp"
			$arr = get_array( 2200, -30, 2250, 10 )		
		Case "img/common/load_error.bmp"	
			$arr = get_array( 460, 100, 750, 190 )
			
		; Events
		Case "img/events/gluecksrad_frei.bmp"
			$arr = get_array( 1279, 655, 1424, 740 )
		Case "img/events/sturm_sektor_frei.bmp"
			$arr = get_array( 586, 378, 744, 515 )
	EndSwitch

	If( IsArray( $arr ) ) Then Return( True )

	get_other_coords( $img_path, $arr )
	If( IsArray( $arr ) ) Then Return( True )

	Debug( "Keine Koordinaten: " & $img_path )
	Return( False )
EndFunc

Func get_other_coords( $img_path, ByRef $arr )
	$arr = 0

	Switch $img_path
		Case "img/_sonst/cats/name.bmp"
			$arr = get_array( 50, 180, 500, 460 )
		Case "img/_sonst/cats/ok.bmp"
			$arr = get_array( 640, 890, 1120, 1200 )
		Case "img/_sonst/cats/duell.bmp"
			$arr = get_array( 1010, 930, 1360, 1070 )
		Case "img/_sonst/cats/nehmen.bmp"
			$arr = get_array( 740, 850, 1150, 1200 )
	EndSwitch
EndFunc
