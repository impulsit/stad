#include "base.au3"

Debug( "--------------------" )
Debug( "--- FRAGEZEICHEN ---" )
Debug( "--------------------" )

load_config_file()

$fight_type = 3 ; Farmen
$bInSektor = True
$iError = 0
$iLastPos = -1
$iLastPosCounter = 0

reset_mouse()
If( $bInSektor ) Then
	switch_to_weapon_offi( $_fragezeichen_weapon1, $_fragezeichen_weapon2, $_fragezeichen_offi, $_fragezeichen_repair )
	If( $_fragezeichen_shuttles ) Then
		load_shuttles( $_fragezeichen_heal_shuttles )
	Else
		unload_shuttles()
	EndIf
	change_weapon_set( $_fragezeichen_weapon_set )
EndIf

; Fragezeichen Koords
dim $k[20]
$k[ 1] = get_array(	600, 500,	900, 700 )
$k[ 6] = get_array( 1000, 300, 1300, 600 )
$k[ 7] = get_array( 1000, 600, 1200, 800 )
$k[ 4] = get_array( 1000, 600, 1200, 800 )
$k[ 2] = get_array(	400, 600,	700, 800 )
$k[ 5] = get_array( 1000, 600, 1200, 800 )
$k[ 3] = get_array(	800, 600, 1000, 800 )
$k[ 8] = get_array( 1000, 600, 1200, 800 )
$k[12] = get_array( 1000, 600, 1200, 800 )
$k[15] = get_array( 1400, 500, 1600, 800 )
$k[14] = get_array( 1400, 600, 1700, 800 )
$k[11] = get_array( 1000, 600, 1200, 800 )
$k[ 9] = get_array( 1000, 600, 1200, 800 )
$k[10] = get_array( 1000, 600, 1200, 800 )
$k[13] = get_array( 1200, 400, 1600, 700 )
$k[16] = get_array(	800, 600, 1100, 800 ) ; Schlachtfeld

; Weiterfliegen Koords
dim $w[16]
$w[ 6] = get_array( 1500, 300, 1800,	500 )
$w[ 7] = get_array( 1200, 800, 1400,	900 )
$w[ 4] = get_array(	100, 700,	400,	900 )
$w[ 2] = get_array(	400, 700,	700,	900 )
$w[ 5] = get_array( 1300, 500, 1500,	700 )
$w[ 3] = get_array(	500, 900,	700, 1100 )
$w[ 8] = get_array( 1800, 400, 1920,	500 )
$w[12] = get_array( 1600, 400, 1900,	600 )
$w[15] = get_array( 1700, 500, 1920,	700 )
$w[14] = get_array( 1400, 300, 1600,	500 )
$w[11] = get_array(	700, 300, 1100,	600 )
$w[ 9] = get_array(	400, 600,	700,	900 )
$w[10] = get_array( 1400, 200, 1600,	400 )
$w[13] = get_array( 1600, 300, 1900,	500 )

$bFZFinished = False
While( (Not $bFZFinished) And (Not event_starting_soon()) )
	check_dc()

	If( search_pattern( "sys/sys_flotte" ) ) Then
		Debug( "[FZ] Center" )
		mouse_move_relative( 0, 0, True )
	EndIf

	$bSektorFinished = False
	$iNextSektor = -1
	If( search_pattern( "frage/sek0_0_0",	 $k[16] ) ) Then $iNextSektor = 20 ; Schlachtfeld
	If( search_pattern( "frage/sek20_13_0", $k[13] ) ) Then $iNextSektor = 18
	If( search_pattern( "frage/sek18_13_0", $k[13] ) ) Then $iNextSektor = 19
	If( search_pattern( "frage/sek19_13_0", $k[13] ) ) Then $iNextSektor = 22
	If( search_pattern( "frage/sek22_13_0", $k[13] ) ) Then $iNextSektor = 23
	If( search_pattern( "frage/sek23_13_0", $k[13] ) ) Then $iNextSektor = 0

	If( $iNextSektor >= 0 ) Then
		If( search_pattern( "frage/worldmap" ) ) Then mouse_move_relative( 0, 0, True )
		wait_sec( 1 )

		$bFound = False
		If( ($iNextSektor = 15) And search_pattern( "frage/sek15" ) ) Then $bFound = True
		If( ($iNextSektor = 16) And search_pattern( "frage/sek16" ) ) Then $bFound = True
		If( ($iNextSektor = 17) And search_pattern( "frage/sek17" ) ) Then $bFound = True
		If( ($iNextSektor = 18) And search_pattern( "frage/sek18" ) ) Then $bFound = True
		If( ($iNextSektor = 19) And search_pattern( "frage/sek19" ) ) Then $bFound = True
		If( ($iNextSektor = 20) And search_pattern( "frage/sek20" ) ) Then $bFound = True
		If( ($iNextSektor = 21) And search_pattern( "frage/sek21" ) ) Then $bFound = True
		If( ($iNextSektor = 22) And search_pattern( "frage/sek22" ) ) Then $bFound = True
		If( ($iNextSektor = 23) And search_pattern( "frage/sek23" ) ) Then $bFound = True
		If( ($iNextSektor =	0) And search_pattern( "frage/sek0" ) ) Then
			$bFound = False
			$bFZFinished = True
		EndIf

		If( $bFound ) Then
			Debug( "[FZ] Fliege -> Sektor " & $iNextSektor )

			mouse_move_relative( 0, 50, True, True )
			wait_sec( 1 )
			mouse_move_relative( -120, 20, True )
			wait_sec( 1 )
		EndIf
	EndIf

	While( (Not $bSektorFinished) And (Not $bFZFinished) And (Not event_starting_soon()) )
		check_dc()

		$bOK = True
		If( search_pattern( "common/flotte_fliegt" ) ) Then
			Debug( "[FZ] Flotte fliegt" )
			$bOK = False
		EndIf

		; Position bestimmen
		If( $bOK ) Then
			center_fleet()

			$iPos = get_current_sys()
			Debug( "[FZ] Pos: " & $iPos )

			If( $iPos = -1 ) Then 
				$bOK = False
				$iError = $iError + 1
				If( $iError > 5 ) Then
					reload_window()
					$iError = 0
				EndIf
			EndIf
		EndIf

		; Aufdecken + Angriff
		If( $bOK ) Then
			wait_sec( 1 )
			$i = 0
			$i = 0
			While( search_pattern( "frage/fragezeichen", $k[$iPos] ) And ($i < 10) )
				$i = $i + 1

				mouse_move_relative( 0, 0, True, True )
				wait_sec( 1.5 )

				If( search_pattern( "frage/angriff" ) ) Then
					 mouse_move_relative( 0, 0, True, True )

					 Debug( "[FZ] Kampf Start" )
					 do_fight( $fight_type )

					 close_all_windows()
					 close_all_windows()

					 $bOK = False
				EndIf
			WEnd
			If( $i >= 10 ) Then 
				reload_window()
			 $bOK = False
			EndIf
		EndIf

		If( $bOK ) Then
			If( $iPos = 13 ) Then
				$bOK = False
				$bSektorFinished = True
			EndIf
			If( ($iPos = 15) And $_fragezeichen_use_deut ) Then
				If( search_pattern( "frage/sek_for_deut", $k[15] ) ) Then
					If( $_fragezeichen_collect_res ) Then
						collect_res()
						wait_sec( 2 )
					EndIf

					If( search_pattern( "frage/sek_for_deut", $k[15] ) ) Then
						Debug( "[FZ] Deuterium" )
						mouse_move_relative( -50, -200, True )
						$i = 0
						wait_sec( 1 )
						If( search_pattern( "frage/saeubern" ) ) Then
							mouse_move_relative( 0, 0, True )
							wait_sec( 0.5 )

							search_pattern( "frage/saeubern" )
							While(	$i < 20 )
								$i = $i + 1
								mouse_move_relative( 0, 0, True )
								wait_sec( 0.1 )
							WEnd
						EndIf

						close_all_windows()

						reset_mouse()
					EndIf
				EndIf
			EndIf
		EndIf

		; Weiterfliegen
		If( $bOK ) Then
			If( $iPos =	1 ) Then $iNextPos = 6
			If( $iPos =	6 ) Then $iNextPos = 7
			If( $iPos =	7 ) Then $iNextPos = 4
			If( $iPos =	4 ) Then $iNextPos = 2
			If( $iPos =	2 ) Then $iNextPos = 5
			If( $iPos =	5 ) Then $iNextPos = 3
			If( $iPos =	3 ) Then $iNextPos = 8
			If( $iPos =	8 ) Then
				$iNextPos = 12
				If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative( -130, 35, True ) ; Links oben
				If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative( -50, 150, True )
				;drag( -300, 0, 100, -100 )
			EndIf
			If( $iPos = 12 ) Then $iNextPos = 15
			If( $iPos = 15 ) Then
				$iNextPos = 14
				If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative( -130, 35, True ) ; Links oben
				If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative( 0, 110, True )
				;drag( 0, -500, 100, -100 )
			EndIf
			If( $iPos = 14 ) Then $iNextPos = 11
			If( $iPos = 11 ) Then $iNextPos = 9
			If( $iPos =	9 ) Then
				$iNextPos = 10
				If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative( -130, 35, True ) ; Links oben
				If( search_pattern( "common/symbol_settings" ) ) Then mouse_move_relative( -45, 80, True )
				;drag( 0, -400, 100, -100 )
			EndIf
			If( $iPos = 10 ) Then $iNextPos = 13

			Debug( "[FZ] Fliege -> " & $iNextPos )
			If( search_pattern( "frage/sek_anfliegen", $w[$iNextPos] ) ) Then
				mouse_move_relative( 0, 0, True )
				$iError = 0
				If( $iLastPos = $iNextPos ) Then
					$iLastPosCounter = $iLastPosCounter + 1
					Debug( "[FZ] Try: " & $iLastPosCounter )
					If( $iLastPosCounter >= 3 ) Then
						reload_window()
						$iLastPosCounter = 0
					EndIf
				Else
					$iLastPos = $iNextPos
					$iLastPosCounter = 0
				EndIf
			Else
				Debug( "[FZ] ERROR: Anfliegen nicht gefunden" )
				reset_mouse()
				$iError = $iError + 1
				If( $iError > 5 ) Then
					reload_window()
					$iError = 0
				EndIf
			EndIf
		EndIf

		wait_sec( 1 )
	Wend
WEnd

move_to_base()
wait_sec( 2 )
reload_window()

Debug( "-------------------------" )
Debug( "--- FRAGEZEICHEN ENDE ---" )
Debug( "-------------------------" )
