#include "base.au3"

Debug( "-----------" )
Debug( "--- 3V3 ---" )
Debug( "-----------" )

load_config_file()

$bInSektor = True
$bIn3v3 = False
$bStop3v3 = False
$bInFight = False

If( search_pattern( "3v3/3v3_einzelspiel", 3 ) ) Then
	$bInSektor = False
	$bIn3v3 = True
EndIf

reset_mouse()
If( $bInSektor ) Then
	switch_to_weapon_offi( $_3v3_weapon1, $_3v3_weapon2, $_3v3_offi, $_3v3_repair )
	If( $_3v3_shuttles ) Then
		load_shuttles( $_3v3_heal_shuttles )
	Else
		unload_shuttles()
	EndIf
	change_weapon_set( $_3v3_weapon_set )
EndIf

arena_store()

While (Not $bStop3v3)
	$hour = Int( @HOUR )
	$min = Int( @MIN )

	If( check_dc() ) Then
		$bInSektor = True
		$bIn3v3 = False
	EndIf

	If( search_pattern( "3v3/3v3_einzelspiel" ) ) Then
		$bInSektor = False
		$bIn3v3 = True
	EndIf
	
	; 3v3 Anmeldung
	If( $bInSektor ) Then
		Debug( "[3v3]3v3 Start" )
		If( search_pattern( "3v3/3v3_start" ) ) Then
			mouse_move_relative( 0, 0, true )
			wait_sec( 3 )
			$bIn3v3 = True
			$bInSektor = False
		EndIf
	EndIf

	; In 3v3 Screen?
	If( $bIn3v3 ) Then
		Debug( "[3v3]Ende" )
		If( search_pattern( "3v3/3v3_ende" ) ) Then $bStop3v3 = True

		If( Not $bStop3v3 ) Then
			Debug( "[3v3]Start" )
			If( search_pattern( "3v3/3v3_start" ) ) Then
				mouse_move_relative( 0, 0, true )
				wait_sec( 3 )
				$bIn3v3 = True
				$bInSektor = False
			EndIf
		
			Debug( "[3v3]Verlassen" )
			If( search_pattern( "kampf/verlassen" ) ) Then
				mouse_move_relative( 0, 0, true )
				$bInFight = False
				wait_sec( 0.5 )
			EndIf
		 
			Debug( "[3v3]Einzelkampf" )
			If( search_pattern( "3v3/3v3_einzelspiel" ) ) Then 
				mouse_move_relative( 0, 0, True )
				wait_sec( 0.5 )
			EndIf

			Debug( "[3v3]Start Abfrage" )
			If( search_pattern( "3v3/3v3_abfrage" ) ) Then
				mouse_move_relative( -10, 130, True )
				$bInFight = True
			EndIf
			
			If( search_pattern( "3v3/3v3_wait" ) ) Then 
				Debug( "[3v3]Setze Autokampf" )
				mouse_move_relative( 50, 0, True )
			EndIf	
		EndIf
	EndIf

	If( ($hour = 9) Or ($hour = 15) Or ($hour = 22) ) Then $bStop3v3 = True

	If( Not $bStop3v3 ) Then wait_sec( 0.5 )
WEnd

If( $bIn3v3 ) Then
	If( $bInFight ) Then
		wait_min( 5 )
		Debug( "[3v3]Verlassen" )
		If( search_pattern( "kampf/verlassen" ) ) Then mouse_move_relative( 0, 0, true )
		wait_sec( 1 )
	EndIf

	close_all_windows()
	
	$bIn3v3 = False
	$bInSektor = True

	reload_window()
EndIf

Debug( "----------------" )
Debug( "--- 3V3 ENDE ---" )
Debug( "----------------" )
