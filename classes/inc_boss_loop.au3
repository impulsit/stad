;#include "base.au3"

Debug( "------------" )
Debug( "--- BOSS ---" )
Debug( "------------" )

load_config_file()

$fight_type = 1 ; Boss
$bInSektor = True
$bInBoss = False
$bStopBoss = False
$iStartTime = 0
$bUsePriority = False
$bReloadTorp = False

If( search_pattern( "boss/in_boss_screen" ) ) Then
	$bInSektor = False
	$bInBoss = True
EndIf

reset_mouse()
If( $bInSektor ) Then
	switch_to_weapon_offi( $_boss_weapon1, $_boss_weapon2, $_boss_offi, $_boss_repair )
	If( $_boss_shuttles ) Then
		load_shuttles( $_boss_heal_shuttles )
	Else
		unload_shuttles()
	EndIf
	change_weapon_set( $_boss_weapon_set )
EndIf

While (Not $bStopBoss)
	$hour = Int( @HOUR )
	$min = Int( @MIN )

	If( check_dc() ) Then
		Debug( "[BOSS] DC" )
		$bInSektor = True
		$bInBoss = False
	EndIf

	; Im Kampf? -> Failsave
	Debug( "[BOSS] Im Kampf?" );
	If( search_pattern( "kampf/att_start" ) ) Then
		$bInBoss = True
		$bInSektor = False

		do_fight( $fight_type )

		; Mouse Reset links oben
		MouseMove( Round( (@DesktopWidth - 1920) / 2 ) + 250, Round( (@DesktopHeight - 1080 ) / 2 ) + 200, 2 )
		MouseClick( "left" )
		wait_sec( 0.5 )
		MouseClick( "left" )
		wait_sec( 0.5 )

		$bReloadTorp = True
	EndIf

	; Boss Anmeldung
	If( $bInSektor ) Then
		Debug( "[BOSS] Suche Boss Start" )
		$bError = False

		If( search_pattern( "boss/boss_start", 0 ) ) Then
			If( (($hour =	6) And ($min	<= 3)) Or (($hour =	5) And ($min >= 59)) ) Then wait_sec( Round( $_wait_sec_06 * 0.7, 0 ), Round( $_wait_sec_06 * 1.3, 0 ) )
			If( (($hour = 12) And ($min	<= 3)) Or (($hour = 11) And ($min >= 59)) ) Then wait_sec( Round( $_wait_sec_12 * 0.7, 0 ), Round( $_wait_sec_12 * 1.3, 0 ) )
			If( (($hour = 19) And ($min	<= 3)) Or (($hour = 18) And ($min >= 59)) ) Then wait_sec( Round( $_wait_sec_19 * 0.7, 0 ), Round( $_wait_sec_19 * 1.3, 0 ) )

			Debug( "[BOSS] Suche Boss Angriff" )
			If( Not wait_for_window( "boss/boss_start", "boss/start_boss_angreifen" ) ) Then $bError = True
			If( (Not $bError) And wait_for_window( "boss/start_boss_angreifen", "boss/in_boss_screen" ) ) Then
				Debug( "[BOSS] in Boss Screen" )
				$bInBoss = True
				$bInSektor = False
				$iStartTime = get_timestamp()
			EndIf
		Else
			$bError = True
		EndIf

		If( $bError ) Then
			If( ($min > 3) And ($min < 45) ) Then
				reload_window()
				;close_all_windows()
				;center_fleet()
			EndIf
		EndIf

		If( search_pattern( "common/fight_continue" ) ) Then
			Debug( "[BOSS] Boss fortsetzen" )
			mouse_move_relative_debug( 0, 0, true )
			$bInBoss = True
			$bInSektor = False
			$iStartTime = get_timestamp()
		EndIf
	EndIf

	If( search_pattern( "boss/boss_korvac_dead" ) ) Then
		Debug( "[BOSS] Boss Tot" )
		$bStopBoss = True
	EndIf

		; In Boss Screen?
	If( $bInBoss ) Then
		If( search_pattern( "boss/buy_att", 1 ) ) Then
			Debug( "[BOSS] Angriff kaufen abbrechen" );
			mouse_move_relative( 90, 60, True )
		EndIf

		$iLast = -1
		If( search_pattern( "boss/last_0"	) ) Then $iLast =	0
		If( search_pattern( "boss/last_1"	) ) Then $iLast =	1
		If( search_pattern( "boss/last_2"	) ) Then $iLast =	2
		If( search_pattern( "boss/last_3"	) ) Then $iLast =	3
		If( search_pattern( "boss/last_4"	) ) Then $iLast =	4
		If( search_pattern( "boss/last_5"	) ) Then $iLast =	5
		If( search_pattern( "boss/last_6"	) ) Then $iLast =	6
		If( search_pattern( "boss/last_7"	) ) Then $iLast =	7
		If( search_pattern( "boss/last_8"	) ) Then $iLast =	8
		If( search_pattern( "boss/last_9"	) ) Then $iLast =	9
		If( search_pattern( "boss/last_10" ) ) Then $iLast = 10
		If( search_pattern( "boss/last_11" ) ) Then $iLast = 11
		If( search_pattern( "boss/last_12" ) ) Then $iLast = 12

		Switch $hour
			Case 6
				$bUsePriority = True
				If( ($iLast = $_stop_boss_06_1) Or ($iLast = $_stop_boss_06_2) ) Then
					Debug( "[BOSS] Nur mehr " & $iLast & " Angriffe -> Stop" );
					$bStopBoss = True
				EndIf
			Case 12
				$bUsePriority = True
				If( ($iLast = $_stop_boss_12_1) Or ($iLast = $_stop_boss_12_2) ) Then
					Debug( "[BOSS] Nur mehr " & $iLast & " Angriffe -> Stop" );
					$bStopBoss = True
				EndIf
			Case 19
				If( ($iLast = $_stop_boss_19_1) Or ($iLast = $_stop_boss_19_2) ) Then
					Debug( "[BOSS] Nur mehr " & $iLast & " Angriffe -> Stop" );
					$bStopBoss = True
				EndIf
		EndSwitch

		If( Not $bStopBoss ) Then
			If( $bReloadTorp ) Then
				reload_torp()
				$bReloadTorp = False
				$bInBoss = False
				$bInSektor = True
			EndIf

			If( search_pattern( "boss/boss_angriff_moeglich" ) ) Then
				$bAnmelden = True
				If( $bUsePriority ) Then
					If( ($min < 30) And search_pattern( "boss/shield_up" ) ) Then
						Debug( "[BOSS] Schild Up" )
						$bAnmelden = False
					EndIf
					If( ($min < 30) And (Not search_pattern( "boss/shield_down" )) ) Then
						Debug( "[BOSS] Schild Not Down" )
						$bAnmelden = False
					EndIf
					If( search_pattern( "boss/place_1" ) ) Then
						Debug( "[BOSS] Keine Anmeldung -> Platz 1" );
						$bAnmelden = False
					EndIf
				EndIf

				If( ($bAnmelden = False) And ($hour = 12) And ($min >= 48) And ($iLast >= 10) ) Then $bAnmelden = True

				If( $bAnmelden ) Then
					If( search_pattern( "boss/boss_angriff_moeglich" ) ) Then
						Debug( "[BOSS] Boss Anmeldung" )
						mouse_move_relative( -200, 0, true )
					EndIf
				EndIf
			EndIf
		EndIf

		If( ($iStartTime < (get_timestamp() - 60)) And (Not search_pattern( "boss/in_boss_screen" )) ) Then
			Debug( "[BOSS] nicht in Boss" )
			$bInSektor = True
			$bInBoss = False
		EndIf
	EndIf

	If( ((($hour = 6) Or ($hour = 12) Or ($hour = 19)) And ($min >= 57)) Or _
			(($hour = 7) Or ($hour = 13) Or ($hour = 20)) ) Then
		$bStopBoss = True
	EndIf

	If( Not $bStopBoss ) Then wait_sec( 0.1 )
WEnd

do_screenshot( "boss" )

If( $bInBoss ) Then
	If( search_pattern( "boss/boss_aufgeben", 0 ) ) Then
		mouse_move_relative( 0, 0, True )
		If( search_pattern( "boss/boss_aufgeben_text", 0 ) ) Then mouse_move_relative( 10, 135, True )
	EndIf
	close_all_windows()
	$bInBoss = False
	$bInSektor = True
EndIf

For $i = 1 To 3
	close_all_windows()
	wait_sec( 1 )
Next
center_fleet()

Debug( "-----------------" )
Debug( "--- BOSS ENDE ---" )
Debug( "-----------------" )
