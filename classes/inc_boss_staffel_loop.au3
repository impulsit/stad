#include "base.au3"

Debug( "-------------------" )
Debug( "--- STAFFELBOSS ---" )
Debug( "-------------------" )

load_config_file()

$fight_type = 1 ; Boss
$bInSektor = True
$bInBoss = False
$bStopBoss = False
$iFail = 0
$iStartTime = 0

If( search_pattern( "boss/in_boss_screen" ) ) Then
	$bInSektor = False
	$bInBoss = True
	$iStartTime = get_timestamp()
EndIf

reset_mouse()
If( $bInSektor ) Then
	switch_to_weapon_offi( $_boss_weapon1, $_boss_weapon2, $_boss_offi, $_boss_repair )
	If( $_boss_shuttles ) Then
		load_shuttles( $_boss_heal_shuttles )
	Else
		unload_shuttles()
	EndIf
	change_weapon_set( $_boss_weapon_set )
EndIf

While (Not $bStopBoss)
	$hour = Int( @HOUR )
	$min = Int( @MIN )

	; Im Kampf? -> Failsave
	Debug( "[BOSS] Im Kampf?" );
	If( search_pattern( "kampf/att_start" ) ) Then
		$bInBoss = True
		$bInSektor = False
		$iStartTime = get_timestamp()

		do_fight( $fight_type )

		; Mouse Reset links oben
		MouseMove( Round( (@DesktopWidth - 1920) / 2 ) + 250, Round( (@DesktopHeight - 1080 ) / 2 ) + 200, 2 )
		MouseClick( "left" )
		wait_sec( 0.5 )
		MouseClick( "left" )
		wait_sec( 0.5 )
	EndIf

	; Boss Anmeldung
	If( $bInSektor ) Then
		Debug( "[BOSS] Suche Boss Start" )
		If( search_pattern( "boss/boss_staffel_start" ) ) Then
			Debug( "[BOSS] Suche Boss Angriff" )
			wait_for_window( "boss/boss_staffel_start", "boss/start_boss_staffel_angreifen" )
			If( wait_for_window( "boss/start_boss_staffel_angreifen", "boss/in_boss_screen" ) ) Then
				Debug( "[BOSS] in Boss Screen" )
				$bInBoss = True
				$bInSektor = False
				$iStartTime = get_timestamp()
			EndIf
		Else
			Debug( "[BOSS] Boss nicht gefunden: " & $iFail )
			$iFail = $iFail + 1
			If( $iFail > 30 ) Then $bStopBoss = True			
		EndIf
	EndIf

	; In Boss Screen?
	If( $bInBoss ) Then
		If( search_pattern( "boss/buy_att", 1 ) ) Then
			Debug( "[BOSS] Angriff kaufen abbrechen" )
			mouse_move_relative( 90, 60, True )
		EndIf

		$iLast = -1
		If( search_pattern( "boss/last_0"	) ) Then $iLast =	0
		If( $iLast = 0 ) Then
			Debug( "[BOSS] Nur mehr " & $iLast & " Angriffe -> Stop" );
			$bStopBoss = True
		EndIf

		If( Not $bStopBoss ) Then
			If( search_pattern( "boss/boss_angriff_moeglich" ) ) Then
				Debug( "[BOSS] Boss Anmeldung" )
				mouse_move_relative( -200, 0, true )
			EndIf
		EndIf
		
		If( ($iStartTime < (get_timestamp() - 60)) And (Not search_pattern( "boss/in_boss_screen" )) ) Then
			Debug( "[BOSS] nicht in Boss" )
			$bInSektor = True
			$bInBoss = False
		EndIf
	EndIf

	If( Not $bStopBoss ) Then wait_sec( 2 )
WEnd

If( $bInBoss ) Then
	do_screenshot( "boss_staffel" )

	close_all_windows()
	$bInBoss = False
	$bInSektor = True
EndIf

For $i = 1 To 3 
	close_all_windows()
	wait_sec( 1 )
Next
center_fleet()

Debug( "------------------------" )
Debug( "--- STAFFELBOSS ENDE ---" )
Debug( "------------------------" )
