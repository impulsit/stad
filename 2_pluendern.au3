#include "classes/base.au3"
#include <File.au3>

; VERSION 1.01, 23.11.16
; -----------------------------------------------------------------------------
; CTRL+F12, CTRL+Q = End
; CTRL+F5, CTRL+ -> = Next Koord
; CTRL+F6, CTRL+ <- = Previous Koord
; -----------------------------------------------------------------------------

HotKeySet("^{F5}", "IncKoord" ) ; CTRL + F5
HotKeySet("^{RIGHT}", "IncKoord" ) ; CTRL + ->
HotKeySet("^{F6}", "DecKoord" ) ; CTRL + F6
HotKeySet("^{LEFT}", "DecKoord" ) ; CTRL + <-

Global $iPointer;
Global $iDirection;
Global $arrKoords;
Global $iMaxArray;

unload_shuttles()

$fight_type = 4 ; Plündern

; Read Data
$iPointer = 0;
$iDirection = 1;
$arrKoords = 0
_FileReadToArray( "data_pluendern.txt", $arrKoords, 0 )
$iMaxArray = Ubound( $arrKoords ) - 1

; -----------------------------------------------------------------------------
While 1
   If( search_pattern( "kampf/att_start" ) ) Then
	  Debug( "[PLUENDERN]Kampf start" );
	  do_fight( $fight_type )
    EndIf

   If( search_pattern( "farm/sektor_verlassen" ) ) Then
	  Debug( "[PLUENDERN]Sektor verlassen" );
	  mouse_move_relative( -70, 140, True )
   EndIf

   wait_sec( 2 )
WEnd

; -----------------------------------------------------------------------------
Func IncKoord()
   If $iDirection = -1 Then
	  $iPointer = $iPointer + 2
   EndIf

   $iDirection = 1
   CheckLimits()

   PasteKoord()
EndFunc

; -----------------------------------------------------------------------------
Func DecKoord()
   If $iDirection = 1 Then
	  $iPointer = $iPointer - 2
   EndIf

   $iDirection = -1
   CheckLimits()

   PasteKoord()
EndFunc

; -----------------------------------------------------------------------------
Func CheckLimits()
   If $iPointer > $iMaxArray Then
	  $iPointer = 0
   EndIf
   If $iPointer < 0 Then
	  $iPointer = $iMaxArray
   EndIf
EndFunc

; -----------------------------------------------------------------------------
Func PasteKoord()
   If( search_pattern( "farm/suche" ) ) Then
	  mouse_move_relative( -30, 0, True )

	  Debug( "[PLUENDERN] -> " & $arrKoords[$iPointer] )
	  Send( "+{HOME}" )
	  Send( $arrKoords[$iPointer] )
	  $iPointer = $iPointer + $iDirection
	  CheckLimits()

	  If( search_pattern( "farm/suche" ) ) Then
		 mouse_move_relative( 0, 0, True )
	  EndIf
   EndIf
EndFunc

; -----------------------------------------------------------------------------
Func End()
   Exit
EndFunc