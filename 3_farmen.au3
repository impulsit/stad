#include "classes/base.au3"

$fight_type = 3 ; Farmen

While 1
   If( search_pattern( "kampf/att_start" ) ) Then
	  Debug( "[FARM]Kampf start" );
	  do_fight( $fight_type )
   EndIf

   If( search_pattern( "farm/sektor_verlassen" ) ) Then
	  Debug( "[FARM]Sektor verlassen" );
	  mouse_move_relative( -70, 140, True )
   EndIf

   If( search_pattern( "farm/flotte_bewegen" ) ) Then
	  Debug( "[FARM]Flotte bewegen" );
 	  mouse_move_relative( -70, 140, True )
   EndIf

   ; Beutemission
   If( search_pattern( "farm/title_beutemission" ) ) Then
	  mouse_move_relative( 220, -60, False )
	  drag_from_to( $_x, $_y, 1635, 345 )
   EndIf

   If( search_pattern( "beute/abschliessen_rechts" ) ) Then mouse_move_relative( 0, 0, True )

   If( search_pattern( "beute/sek_angriff" ) ) Then mouse_move_relative( 0, 0, True )
   If( search_pattern( "beute/drohne" ) ) Then mouse_move_relative( 0, 0, True )
   If( search_pattern( "beute/detektor" ) ) Then mouse_move_relative( 0, 0, True )

   wait_sec( 1 )
WEnd
