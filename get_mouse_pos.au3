#include <Misc.au3>

HotKeySet("^q", "end_script") ; CTRL + q
HotKeySet("^{F12}", "end_script") ; CTRL + F12

$iOffsetX = Round( (@DesktopWidth - 1920) / 2, 0 )
$iOffsetY = Round( (@DesktopHeight - 1080) / 2, 0 )
$oldstr = ""
Local $oldaPos[2]
$oldaPos[0] = 0
$oldaPos[1] = 0

$dll = DllOpen("user32.dll")
While 1
		sleep(10)
		If _IsPressed("01", $dll)	Then
			$aPos = MouseGetPos()
			;$str = "Left: " & ($aPos[0]-$iOffsetX) & " Top: " & ($aPos[1]-$iOffsetY) & @CRLF
			If( ($oldaPos[0] <> $aPos[0]) And ($oldaPos[1] <> $aPos[1]) ) Then
				$str = "$arr = get_array( " & ($oldaPos[0]-$iOffsetX) & ", " & ($oldaPos[1]-$iOffsetY) & ", " & ($aPos[0]-$iOffsetX) & ", " & ($aPos[1]-$iOffsetY) & " )" & @CRLF
				$oldaPos = $aPos
			EndIf

			If( $str <> $oldstr ) Then
				Consolewrite( $str )
				$oldstr = $str
			EndIf
		EndIf
WEnd
DllClose($dll)

Func end_script()
	Exit
EndFunc
