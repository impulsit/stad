#include "classes/base.au3"

$iLastBoss = -1
$iLastBossStaffel = -1
$iLastHeraus = -1
$iLastInter = -1
$iLastTrials = -1
$iLastSys = -1
$iLast3v3 = -1
$iLastRes = -1
$iLastShuttle = -1
$iLastSendShuttles = -1
$iLastFZ = -1
$iLastBeute = -1
$iLastStaffel = -1

$iLastDay = Int( @MDAY )
Local $iii = 0
Local $iii2 = 0

While 1
	$hour = Int( @HOUR )
	$min = Int( @MIN )
	$day = Int( @MDAY )

	check_dc()

	If( ($hour = 0) And ($min = 10) ) Then
		reload_window()
		wait_sec( 60 )
	EndIf

	; Alles OK?
	$bOK = True
	If( Not search_pattern( "frage/sek0_0_0", get_array(	800, 600, 1100, 800 ) ) ) Then
		$bOK = False

		If( search_pattern( "common/flotte_fliegt" ) ) Then
			Debug( "[FZ] Flotte fliegt: " & $iii2 )
			$iii2 = $iii2 + 1

			If( $iii2 > 60 ) Then
				do_screenshot( "unknown" )

				reload_window()
				$iii2 = 0
			EndIf
		Else
			Debug( "[ERROR] Fehler bei Position: " & $iii )
			$iii = $iii + 1
			If( $iii > 10 ) Then reset_mouse()
			If( $iii > 20 ) Then move_to_base()
			If( $iii > 30 ) Then
				do_screenshot( "unknown" )

				reload_window()
				$iii = 0
			EndIf
		EndIf
	Else
		$iii = 0
		$iii2 = 0
	EndIf

	; Restart von Sys immer OK
	If( (($hour = 7) Or ($hour = 13) Or ($hour = 20)) And ($min <	40) ) Then $bOK = True

	If( $bOK ) Then
		If( $day <> $iLastDay ) Then
			$iLastDay = $day
			$iLastBoss = -1
			$iLastHeraus = -1
			$iLastInter = -1
			$iLastSys = -1
			$iLast3v3 = -1
			$iLastRes = -1
			$iLastShuttle = -1
			$iLastSendShuttles = -1
			$iLastFZ = -1
			$iLastBeute = -1
			$iLastTrials = -1
			$iLastBossStaffel = -1

			;collect_events()
		EndIf

		; -----------------------------------------------------------------------------
		; STAFFEL
		; -----------------------------------------------------------------------------
		If( ($day <> $iLastStaffel) And ($hour >= 6) And ($min >= 10) And ($min <= 50) ) Then
			$iLastStaffel = $day

			collect_staffel()
			;collect_events()
		EndIf

		; -----------------------------------------------------------------------------
		; RES
		; -----------------------------------------------------------------------------
		If( ($min >= 10) And ($min <= 50) And ($hour <> -1) And ($iLastRes <> $hour) ) Then
			If( Not search_pattern( "sys/sys_start_into" ) ) Then
				$iLastRes = $hour
				Debug( "------------" )
				Debug( "--- RESS ---" )
				Debug( "------------" )

				collect_res()
				collect_online()
				collect_css_res()
				do_arena_att()
				collect_messages()
				collect_daily()
				;open_signs()

				If( $hour = 0 ) Then reload_window()
				$hour = -1

				Debug( "-----------------" )
				Debug( "--- RESS ENDE ---" )
				Debug( "-----------------" )
			EndIf
		EndIf

		; -----------------------------------------------------------------------------
		; FRACHT
		; -----------------------------------------------------------------------------
		$sh_hour = Floor( $hour / 2 )
		$temp_hour = (2 * $hour + Floor( $min / 30 )) / 2
		If( ((($min >= 3) And ($min <= 27)) Or (($min >= 33) And ($min <= 57))) And ($hour <> -1) And ($iLastSendShuttles <> $temp_hour) ) Then
			If( Not search_pattern( "sys/sys_start_into" ) ) Then
				$iLastSendShuttles = $temp_hour
				send_shuttles()
				$hour = -1
			EndIf
		EndIf

		; -----------------------------------------------------------------------------
		; STAFFELBOSS
		; -----------------------------------------------------------------------------
		If( ($hour <> -1) And ($iLastBossStaffel <> $day) And search_pattern( "boss/boss_staffel_start" ) ) Then
			$iLastBossStaffel = $day

			#include "classes/inc_boss_staffel_loop.au3"
			$hour = -1
		EndIf

		; -----------------------------------------------------------------------------
		; BOSS
		; -----------------------------------------------------------------------------
		If( ( _
				((($hour = 6) Or ($hour = 12) Or ($hour = 19)) And ($min <= 56)) Or _ ; Boss
				((($hour = 5) Or ($hour = 11) Or ($hour = 18)) And ($min >= 57)) _		; Pre-Boss
			) And ($hour <> -1) And ($iLastBoss <> $hour) ) Then
			$iLastBoss = $hour
			If( ($iLastBoss = 5) Or ($iLastBoss = 11) Or ($iLastBoss = 18) ) Then $iLastBoss = $iLastBoss + 1

			#include "classes/inc_boss_loop.au3"
			$hour = -1
		EndIf

		; -----------------------------------------------------------------------------
		; SYS
		; -----------------------------------------------------------------------------
		If( ( _
			((($hour = 7) Or ($hour = 13) Or ($hour = 20)) And ($min <= 36)) Or _ ; Sys
			((($hour = 6) Or ($hour = 12) Or ($hour = 19)) And ($min >= 57)) _		; Pre-Sys
			) And ($hour <> -1) And ($iLastSys <> $hour) ) Then
			$iLastSys = $hour
			If( ($iLastSys = 6) Or ($iLastSys = 12) Or ($iLastSys = 19) ) Then $iLastSys = $iLastSys + 1

			#include "classes/inc_sys_fly_loop.au3"
			$hour = -1
		EndIf

		; -----------------------------------------------------------------------------
		; FRAGEZEICHEN
		; -----------------------------------------------------------------------------
		If( ( _
			((($hour = 7) Or ($hour = 13) Or ($hour = 20)) And ($min >= 40)) _ ; nach Sys
			) And ($hour <> -1) And ($iLastFZ <> $hour) ) Then
			$iLastFZ = $hour

			#include "classes/inc_frage_loop.au3"
			$hour = -1
		EndIf

		; -----------------------------------------------------------------------------
		; 3V3
		; -----------------------------------------------------------------------------
		If( ( _
			(($hour =	8) And ($min >= 35)) Or _
			(($hour = 14) And ($min >= 35)) Or _
			(($hour = 21) And ($min >= 35)) _
			) And ($iLast3v3 <> $hour) ) Then
			$iLast3v3 = $hour

			#include "classes/inc_3v3_loop.au3"
			$hour = -1
		EndIf

		; -----------------------------------------------------------------------------
		; INTERSTELLAR
		; -----------------------------------------------------------------------------
		If( (($hour = 9) Or ($hour = 15) Or ($hour = 22) Or ($hour = 2)) And ($hour <> -1) And ($iLastInter <> $hour) ) Then
			$iLastInter = $hour

			#include "classes/inc_interstellar_loop.au3"
			$hour = -1
		EndIf

		; -----------------------------------------------------------------------------
		; TRIALS
		; -----------------------------------------------------------------------------
		If( (($hour = 9) Or ($hour = 15) Or ($hour = 22) Or ($hour = 2)) And ($hour <> -1) And ($iLastTrials <> $hour) ) Then
			$iLastTrials = $hour

			#include "classes/inc_trials_loop.au3"
			$hour = -1
		EndIf

		; -----------------------------------------------------------------------------
		; HERAUSFORDERUNGEN
		; -----------------------------------------------------------------------------
		If( (($hour = 9) Or ($hour = 15) Or ($hour = 22) Or ($hour = 2)) And ($hour <> -1) And ($iLastHeraus <> $hour) ) Then
			$iLastHeraus = $hour

			#include "classes/inc_heraus_loop.au3"
			$hour = -1
		EndIf

		; -----------------------------------------------------------------------------
		; BEUTEMISSION
		; -----------------------------------------------------------------------------
		If( (($hour = 9) Or ($hour = 15) Or ($hour = 22)) And ($hour <> -1) And ($iLastBeute <> $hour) ) Then
			$iLastBeute = $hour

			#include "classes/inc_beute_loop.au3"
			$hour = -1
		EndIf

		; -----------------------------------------------------------------------------
		; SHUTTLE
		; -----------------------------------------------------------------------------
		If( (($hour = 10) Or ($hour = 16) Or ($hour = 17) Or ($hour = 23) Or ($hour = 3) Or ($hour = 4)) And ($hour <> -1) And ($iLastShuttle <> $hour) ) Then
			$iLastShuttle = $hour

			#include "classes/inc_shuttle_loop.au3"
			$hour = -1
		EndIf

		write_debug_window_data(	0, $iLastBoss & " / " & $iLastBossStaffel )
		write_debug_window_data(	1, $iLastSys )
		write_debug_window_data(	2, $iLast3v3 )
		write_debug_window_data(	3, $iLastRes )
		write_debug_window_data(	4, $iLastHeraus )
		write_debug_window_data(	5, $iLastShuttle )
		write_debug_window_data(	6, $iLastInter & " / " & $iLastTrials )
		write_debug_window_data(	7, $iLastFZ )
		write_debug_window_data(	8, $iLastBeute )
		write_debug_window_data(	9, $iLastSendShuttles )
	EndIf

	Sleep( 2000 )
WEnd
